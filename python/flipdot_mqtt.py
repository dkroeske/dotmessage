import sys
import paho.mqtt.client as mqtt
import json
import time
import io
import fonts


batteryIcon = [
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xf8, 0x40, 0x08, 0x5b, 0x6e, 0x5b, 0x6e, 0x5b, 0x6e,
    0x5b, 0x6e, 0x5b, 0x6e, 0x5b, 0x6e, 0x40, 0x08, 0x7f, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
]

class Bitmap(object):
    """

    """

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.bitmap = [[0 for _ in range(self.height)] for _ in range(self.width)]

    def icon_16x16(self, iconMap, offset_x = 0, offset_y = 0):
        for i in range(batteryIcon.__len__()):
            x = i % 2
            y = i // 2  # Integer divide in Pythone 3.x
            b = iconMap[i]
            for j in range(16):
                if 0x80 == 0x80 & (b << j):
                    self.setPixel(x * 8 + j + offset_x, y + offset_y)
                else:
                    self.clrPixel(x * 8 + j + offset_x, y + offset_y)


    def bigNumber(self, char, offset_x = 0, offset_y = 0):
        digit_ar = fonts.tahoma_16pt[ ord(char) - ord('0') ]

        for i in range(digit_ar.__len__()):
            x = i % 2
            y = i // 2
            b = digit_ar[i]
            for j in range(8):
                if 0x80 == 0x80 & (b << j):
                    self.bitmap[x * 8 + j + offset_x][y + offset_y] = 1
                else:
                    self.bitmap[x * 8 + j + offset_x][y + offset_y] = 0


    def char_8x8(self, char, offset_x = 0, offset_y = 0):
        digit_ar = fonts.unscii[ ord(char)  ]

        for x in range(digit_ar.__len__()):
            b = digit_ar[x]
            for y in range(8):
                if 0x80 == 0x80 & (b << y):
                    self.setPixel(x + offset_x, 8-y + offset_y)
                else:
                    self.clrPixel(x + offset_x, 8-y + offset_y)

    def text_8x8(self, str, offset_x = 0, offset_y = 0):
        for idx, ch in enumerate(str):
            self.char_8x8( ch, offset_x + idx * 8, offset_y )

    def char_16x8(self, char, offset_x=0, offset_y=0):
        digit_ar = fonts.unscii_16[ord(char)]
        for x in range(digit_ar.__len__()):
            b = digit_ar[x]
            for y in range(16):
                if 0x8000 == 0x8000 & (b << y):
                    self.setPixel(x + offset_x, 16-y + offset_y)
                else:
                    self.clrPixel(x + offset_x, 16-y + offset_y)

    def text_16x8(self, str, offset_x=0, offset_y=0):
        for idx, ch in enumerate(str):
            self.char_16x8(ch, offset_x + idx * 8, offset_y)

    def setPixel(self, x, y):
        if x >= 0 and x < self.width and y >=0 and y < self.height:
            self.bitmap[x][y] = 1

    def clrPixel(self, x, y):
        if x >= 0 and x < self.width and y >=0 and y < self.height:
            self.bitmap[x][y] = 0

    def clr(self):
        self.bitmap = [[0x00 for _ in range(self.height)] for _ in range(self.width)]

    def set(self):
        self.bitmap = [[0xFF for _ in range(self.height)] for _ in range(self.width)]

    def debug(self):
        for y in range(16):
            for x in range(28*4):
                b = self.bitmap[x][y]
                if( b == 0 ):
                    print('.', end=""),
                else:
                    print('*', end=""),
            print()

    def __str__(self):

        pixels = []

        # Construct pixel array
        for y in range(int(self.height / 8)):
            for x in range(self.width):
                data = 0
                for i in range(8):
                    b = self.bitmap[x][y * 8 + i]
                    data = data | (b << i)
                pixels.append(data)

        # Construct object
        packed_data = {}
        packed_data['width'] = self.width
        packed_data['height'] = self.height
        packed_data['pixels'] = pixels

        return json.dumps(packed_data)

bitmap = Bitmap(width=28*4, height=16)

def on_connect(mqttc, obj, flags, rc):
    print("rc: " + str(rc))


def on_message(mqttc, obj, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    print(string)

client = mqtt.Client("", clean_session=True)
client.on_message = on_message
client.on_connect = on_connect
client.on_publish = on_publish
client.on_subscribe = on_subscribe
client.on_log = on_log
#client.username_pw_set(username='emon-user', password='emon-password')
client.connect("test.mosquitto.org", 1883, 60)

for t in range(3):

    bitmap.clr()

    bitmap.icon_16x16(batteryIcon, 0, 0)
    bitmap.text_16x8("YOUR CAR IS", 20, 0)
    client.publish("DotMessage/HFD-401DA0D8/raw", bitmap.__str__())
    bitmap.debug()
    time.sleep(3)

    bitmap.clr()
    bitmap.text_16x8("FULLY CHARGED", 0, 0)
    client.publish("DotMessage/HFD-401DA0D8/raw", bitmap.__str__())
    bitmap.debug()
    time.sleep(3)

bitmap.clr()
client.publish("DotMessage/HFD-401DA0D8/raw", bitmap.__str__())


