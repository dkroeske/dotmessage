var express = require('express');
var router = express.Router();

// Fall back, display some info
router.all('*', function (req, res) {
    res.status(200);
    res.json({
        "description": "Project DotMessage API version 1. Please use API version 2"
    });
});


module.exports = router;