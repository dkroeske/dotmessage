const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

//
// Check Production vs Development mode
//
let dbUrl = '';
if( process.env.DOTMESSAGE_NODE_SERVER === 'production') {
    const user = process.env.DOTMESSAGE_MONGODB_USER;
    const pwd = process.env.DOTMESSAGE_MONGODB_PASSWORD;
    dbUrl = 'mongodb://' + user + ':' + pwd + '@localhost:27017/dotmessage';
} else {
    dbUrl = 'mongodb://localhost/dotmessage';
}

//
// Export connection
//
dbConnector = function(mongoUri) {

    return mongoose.connect( mongoUri, {
        //useMongoClient: true,
    })
    .then( db => {
        console.log('Connected ' + dbUrl);
    })
    .catch( error => {
        console.warn('Warning', error.toString());
        throw error;
    });

}(dbUrl);

module.exports = dbConnector;
