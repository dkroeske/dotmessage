var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app.js');

chai.should();

chai.use(chaiHttp);

describe('Message', function() {

    it('It should create user Diederich and post a message (POST)', function (done) {
        chai.request(server)
            .post('/apiv2/message')
            .send({
                'message' : 'It is a nice day',
                'email' : 'dkroeske@gmail.com',
                'name' : 'Diederich'
            })
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                done();
            })
    });


    it('It should create user Pietje Puk and post a message (POST)', function (done) {
        chai.request(server)
            .post('/apiv2/message')
            .send({
                'message' : 'Vandaag is rood',
                'email' : 'ppuk@hotmail.com',
                'name' : 'Pietje Puk'
            })
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                done();
            })
    });


    it('It should list all messages (GET)', function (done) {
        chai.request(server)
            .get('/apiv2/message')
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array');
                done();
            })
    });


    it('It should list all messages for user \'Diederich\' (GET)', function (done) {
        chai.request(server)
            .get('/apiv2/message?/name=diederich')
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array');
                done();
            })
    });


    it('It should handle unknown endpoints (GET)', function (done) {
        chai.request(server)
            .get('/apiv2/messages')
            .end(function (err, res) {
                res.should.have.status(500);
                res.body.should.be.a('object');
                done();
            })
    });

});
