const express = require('express');
const router = express.Router();
const DotMessage = require('./models/dotMessage');
const mqtt = require('mqtt');
const mqttBroker = require('./config.json').mqttBroker;
const ws = require('./dotmessage-ws').ws();

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

//
//
//
const mqttClient = mqtt.connect( mqttBroker );

//
//
//
mqttClient.on('connect', () => {
    console.log('mqtt connected');
});

//
//
//
router.all('*', (req, res, next) => {
    //console.log('apiv2 router.all');
    next();
});

//
//
//
router.post('/message', (req, res) => {

    const dotMessage = new DotMessage(req.body);

    dotMessage.save()
        .then( (data) => {

            // MQTT Publish
            let mqttPayLoad = {};
            mqttPayLoad.message = data.message;
            mqttPayLoad.backlight = 1;
            let mqttTopic = "DotMessage/" + data.signature + "/msg";
            mqttClient.publish( mqttTopic, JSON.stringify( mqttPayLoad ), { qos: 0 });

            // WS broadcast to all clients
            let signature = data.signature;
            ws.in(signature).emit('DotMessage', data.toJSON());

            // Return
            res.status(200).json({'status': 'ok'});
        })
        .catch( error => {
            console.log(error.toString());
            res.status(500).json({"err":"server error"})
        });

});

//
//
//
router.get('/message', (req, res) => {

    const name = req.query.name;
    const limit = Math.min((+req.query.limit || 5), 25);
    const signature = req.query.signature;

    // Build query
    let query = {}

    if( name ) {
        query.name = name;
    }

    if( signature ) {
        query.signature = signature;
    }

    // Query using Mongoose Model.
    DotMessage
        .find(query, {'_id':0, 'name':1, 'email':1, 'message':1, 'signature':1, 'createdAt':1})
        .sort('-createdAt')
        .limit(limit)
        .then( result => {
            res.status(200).json(result);
        })
        .catch( error => {
            res.status(500).json({'error':error.toString()});
        });
});

/**
 * catch all
 */
router.all('*', (req, res) => {
    res.status(500);
    res.json({});
});

module.exports = router;
