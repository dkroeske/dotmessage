const sio = require('socket.io')({transport:['websocket'],});

module.exports.ws = function() {
    return sio;
};

module.exports.initialize = function(port) {

    sio.attach(port);

    sio.on('connection', (socket) => {

        let signature = socket.handshake.query.signature;
        let id = socket.id;

        // Create or join a group with signature
        socket.join(signature);

        console.log('WS client connect with id: ' + id + ' and signature: ' + signature );

        socket.on('disconnect', () => {
            console.log('Ws client disconnect');
        })
    });
};
