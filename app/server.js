const express       = require('express');
const cors          = require('cors');
const path          = require('path');
const bodyParser    = require('body-parser');
const fs            = require('fs');
const morgan        = require('morgan');
const routes_apiv1  = require('./server/routes_apiv1');
const routes_apiv2  = require('./server/routes_apiv2');
const settings      = require('./server/config.json');
const rfs           = require('rotating-file-stream');
const app           = express();
const _             = require('./server/database/db-connector');
const ws            = require('./server/dotmessage-ws');


app.set('webPort', settings.webPort);
app.set('mqttBroker', settings.mqttBroker);

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(cors());

// Set up Morgan logger to file (rotate)
const logDirectory = path.join(__dirname, 'log');
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
const accessLogStream = rfs('access.log', {
  interval: '1d',
  path: logDirectory
});

app.use(morgan('combined', {stream: accessLogStream}));

// Middelware, logging voor alle request
app.all('*', function (req, res, next) {
  next();
});

// Angular DIST output folder
app.use(express.static(path.join(__dirname, 'dist')));

// Routing with versions
app.use('/apiv1', routes_apiv1);
app.use('/apiv2', routes_apiv2);

// Middleware statische bestanden (HTML, CSS, images)
app.get('*', function(reg, res) {
 res.sendFile(path.join(__dirname, '/dist/index.html'))
});

// Start websockets
ws.initialize(8081);

// Start server
var port = process.env.PORT || app.get('webPort');
var server = app.listen(port, function () {
  console.log('The magic happens at port ' + server.address().port);
});

module.exports = app;
