export class Power {
  constructor(
    public value: number,
    public unit: string = 'W') {
  }
}
