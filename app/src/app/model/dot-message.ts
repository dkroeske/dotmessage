export class DotMessage {
  constructor(
    public name: string,
    public email: string,
    public message: string,
    public signature: string,
    public createdAt: string = '' ) {
  }
}
