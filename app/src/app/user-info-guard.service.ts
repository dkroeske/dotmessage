import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterState, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {UserInfoService} from './services/user-info.service';

@Injectable()
export class UserInfoGuardService implements CanActivate {

  constructor( private router: Router, private userInfoService: UserInfoService ) {}

  canActivate(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const userInfo = this.userInfoService.get();
    if (userInfo) {
      return new Promise(
        (resolve, reject) => {
          setTimeout(() => {
            resolve(true);
          }, 250);
        }
      );
    } else {
      this.router.navigate(['/']);
    }
  }
}
