import {Component, OnInit } from '@angular/core';
import {UserInfoService} from '../services/user-info.service';
import {UserInfo} from '../model/user-info';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {

  signupForm: FormGroup;

  constructor(private userInfoService: UserInfoService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {

    this.signupForm = new FormGroup({
      'username': new FormControl(null, [Validators.required, this.valideUsername.bind(this)]),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'signature': new FormControl(null, [Validators.required, this.valideSignature.bind(this)])
    });

    // Fill in, if any
    const userInfo = this.userInfoService.get();

    if ( userInfo != null ) {
      this.signupForm.patchValue({username: userInfo.username});
      this.signupForm.patchValue({email: userInfo.email});
      this.signupForm.patchValue({signature: userInfo.signature});
    }
  }

  valideUsername(control: FormControl): {[s: string]: boolean} {
    const username = control.value;
    const regexp = new RegExp('^[a-zA-Z](.{2,10})\\b$');
    const test = regexp.test(username);
    if (regexp.test(username) !== true) {
      return {username: false};
    } else {
      return null;
    }
  }

  valideSignature(control: FormControl): {[s: string]: boolean} {
    const signature = control.value;
    const regexp = new RegExp('^([A-Z]{3}-[0-9A-Z]{8})\\b$');
    if (regexp.test(signature) !== true) {
      return {signature: false};
    } else {
      return null;
    }
  }

  onSubmit() {
    const userInfo = new UserInfo(
      this.signupForm.value['username'],
      this.signupForm.value['email'],
      this.signupForm.value['signature']
    );
    this.userInfoService.set(userInfo);
    this.router.navigate(['/messages']);
  }

}
