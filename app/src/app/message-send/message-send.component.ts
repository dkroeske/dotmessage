import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {DotMessageService} from '../services/dot-message.service';
import {DotMessage} from '../model/dot-message';

@Component({
  selector: 'app-message-send',
  templateUrl: './message-send.component.html',
  styleUrls: ['./message-send.component.css']
})
export class MessageSendComponent implements OnInit {

  messageForm: FormGroup;

  constructor(private dotMessageService: DotMessageService) { }

  ngOnInit() {
    this.messageForm = new FormGroup({
      'message': new FormControl('message',
        [Validators.required, Validators.minLength(2), Validators.maxLength(30)])
    });
  }

  //
  //
  //
  onSubmit() {
    const msg = this.messageForm.value['message'];
    this.dotMessageService.sendMsg(msg)
      .subscribe(
        (response) => {
          //console.log(response);
        },
        (error) => {
          console.log(error);
        }
      );
  }

}
