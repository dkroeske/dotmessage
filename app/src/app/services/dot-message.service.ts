import { Injectable} from '@angular/core';
import {Http, Headers, Response, RequestOptions, URLSearchParams} from '@angular/http';
import {DotMessage} from '../model/dot-message';
import { Subject } from 'rxjs/Subject';
import {UserInfoService} from './user-info.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class DotMessageService {

  dotMessage = new Subject<DotMessage>();

  constructor(private http: Http, private userInfoService: UserInfoService) {}

  //
  // Get latest messages
  //
  getMsg(): Observable<DotMessage[]> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    const signature = this.userInfoService.get().signature;
    //const url = 'http://localhost:8080/apiv2/message?signature=' + signature + '&limit=3';
    const url = '/apiv2/message?signature=' + signature + '&limit=3';

    return this.http.get(url, options)
      .map((res: Response) => {
        const messages: DotMessage[] = [];
        for (const m of res.json()) {
          const msg = new DotMessage(m.name, m.email, m.message, m.signature, m.createdAt);
          messages.push(msg);
        }
        return messages;
      });
  }


  //
  // Post message
  //
  sendMsg(message: string) {
    const userInfo = this.userInfoService.get();

    // Create JS payload object
    const payload = {
      name: userInfo.username,
      email: userInfo.email,
      message: message,
      signature: userInfo.signature };

    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions( { headers: headers} );
    //return this.http.post('http://localhost:8080/apiv2/message', JSON.stringify(payload), options);
    return this.http.post('/apiv2/message', JSON.stringify(payload), options);
   }
}
