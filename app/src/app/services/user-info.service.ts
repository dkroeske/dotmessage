import { Injectable} from '@angular/core';
import 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import {UserInfo} from '../model/user-info';

@Injectable()
export class UserInfoService {

  constructor() {
  }

  set(userInfo: UserInfo) {
    localStorage.setItem('USER_INFO', JSON.stringify(userInfo));
  }

  get() {
    return JSON.parse(localStorage.getItem('USER_INFO'));
  }

}
