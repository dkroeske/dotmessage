import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { DotMessageService } from './services/dot-message.service';
import { HttpModule} from '@angular/http';
import { MessageSendComponent} from './message-send/message-send.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MessagesComponent } from './messages/messages.component';
import { MessageItemComponent } from './message-item/message-item.component';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { UserInfoComponent } from './user-info/user-info.component';
import { AppRoutingModule } from './app-routing-module';
import { UserInfoService} from './services/user-info.service';
import { UserInfoGuardService} from './user-info-guard.service';
import { ReversePipe} from './pipes/reverse.pipe';
import { Angular2FontawesomeModule} from 'angular2-fontawesome';


@NgModule({
  declarations: [
    AppComponent,
    MessageSendComponent,
    MessagesComponent,
    MessageItemComponent,
    UserInfoComponent,
    ReversePipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    Angular2FontawesomeModule,
  ],
  providers: [DotMessageService, UserInfoService, UserInfoGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
