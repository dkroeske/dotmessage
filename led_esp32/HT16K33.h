#include <Wire.h>

typedef enum {
  ON, OFF
} PIXEL;

class ht16k33 {

public:
  ht16k33();
  void clear();
  void setPixel(int x, int y, PIXEL pixel);
  void drawText(const char *str, uint8_t x, uint8_t y);
  void display();
  void display(int x_offset, int y_offset);
  void animateCircle();

private:
  void tx(uint8_t slave_address, uint8_t data);
  void tx(uint8_t slave_address, uint8_t *data, uint8_t lenght);
  void init(uint8_t slave_address);
  void drawChar(char ch, uint8_t x, uint8_t y);
};
