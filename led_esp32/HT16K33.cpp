#include "ht16k33.h"
#include "font.h"

#define HEIGHT   8
#define WIDTH   8

#define SDA_PIN 21
#define SCL_PIN 22

#define D2_I2C_ADDR (0x70 + 2)
#define D1_I2C_ADDR (0x70 + 5)
#define D0_I2C_ADDR (0x70 + 0)

#define DISPLAY_SIZE 40
#define LEDBOARDS     3

// Physical (LED matrix buffer)
uint8_t ledMatrixBuf[HEIGHT * WIDTH * LEDBOARDS / 8];

// Display buffer, 8 colomns of 8 bits * DISPLAY_SIZE
uint8_t displayBuf[HEIGHT * WIDTH * DISPLAY_SIZE / 8];

/******************************************************************/
ht16k33::ht16k33()
/*
short:         Write buffer to display
inputs:        
outputs:    -
notes:         Init display
Version :      DMK, Initial code
*******************************************************************/
{
  Wire.begin(SDA_PIN, SCL_PIN);

  init(D0_I2C_ADDR);
  init(D1_I2C_ADDR);
  init(D2_I2C_ADDR);
}

 

/******************************************************************/
void ht16k33::drawText(const char *str, uint8_t x, uint8_t y)
/*
short:         Write buffer to display
inputs:        
outputs:    -
notes:         Init display
Version :      DMK, Initial code
*******************************************************************/
{
  char *ptr = (char *) str;
  int offset = 0;
  while( *ptr != '\0' )
  {
    drawChar( *ptr, x + offset, y);
    ptr++;
    offset += 8;
  }
}


/******************************************************************/
void ht16k33::setPixel(int x, int y, PIXEL pixel)
/*
short:         Write buffer to display
inputs:        
outputs:    -
notes:         Init display
Version :      DMK, Initial code
*******************************************************************/
{
  if(x < WIDTH * DISPLAY_SIZE && x >= 0 && y < HEIGHT && y >= 0) {
    switch(pixel) {
       case ON:
          displayBuf[x] |= (1<<y);
          break;
       case OFF:
          displayBuf[x] &= ~(1<<y);
          break;
    }
  }
}

/******************************************************************/
void ht16k33::display()
/*
short:         Write buffer to display
inputs:        
outputs:    -
notes:         Init display
Version :      DMK, Initial code
*******************************************************************/
{  
  display(0,0);
}

/******************************************************************/
void ht16k33::clear()
/*
short:         Clear buffer
inputs:        
outputs:    -
notes:         Init display
Version :      DMK, Initial code
*******************************************************************/
{  
  for( uint16_t idx = 0; idx < (HEIGHT * WIDTH * DISPLAY_SIZE / 8); idx++ ) {
    displayBuf[idx] = 0x00;
  }
}

/******************************************************************/
void ht16k33::display(int x_offset, int y_offset)
/*
short:         Write text to display
inputs:        
outputs:    -
notes:         Init display
Version :      DMK, Initial code
*******************************************************************/
{  
  // Copy display buffer to matrix buffer
  for( uint8_t idx = x_offset; idx < (LEDBOARDS * 8 + x_offset); idx++ ) {
    ledMatrixBuf[idx-x_offset] = displayBuf[idx];
  }

  // Display matrix buffer on ledmatrix
  uint8_t addr;
  for( uint8_t idt = 0; idt < LEDBOARDS; idt++) {
    switch(idt) {
      case 0: addr = D0_I2C_ADDR; break;
      case 1: addr = D1_I2C_ADDR; break;  
      case 2: addr = D2_I2C_ADDR; break;
      default: break;
    }
    Wire.beginTransmission(addr);
    Wire.write(0x00);
    for( uint8_t idx = 0; idx < 8; idx++ ) {
      uint8_t a = ledMatrixBuf[ (idt * 8) + 7 - idx];
      uint8_t data = (a >> 1) | ((a<<7) & 0x80);
      Wire.write( data);
      Wire.write( 0x00);
    }
    Wire.endTransmission();
  }
}


/******************************************************************/
void ht16k33::drawChar(char ch, uint8_t x, uint8_t y)
/*
short:         Draw character in font
inputs:        
outputs:    -
notes:         Init display
Version :      DMK, Initial code
*******************************************************************/
{
   for(uint8_t idx = 0; idx < 8; idx++)
   {
      uint8_t b = pgm_read_byte( funny[ch]+idx );

      for(uint8_t idy = 0; idy < 8; idy++) {
         if( b & (1<<idy) ) {
            setPixel(idx + x, idy + y, ON);
         } else {
            setPixel(idx + x, idy + y, OFF);
         }
      }
   }
}

/******************************************************************/
void ht16k33::init(uint8_t slave_address)
/*
short:         Init display
inputs:        
outputs:    -
notes:         Init display
Version :      DMK, Initial code
*******************************************************************/
{
  tx(slave_address, 0x21);  // Internal osc on (page 10 HT16K33)
  tx(slave_address, 0xA0);  // HT16K33 pins all output (default)
  tx(slave_address, 0xE1);  // Display Dimming 2/16 duty cycle
  tx(slave_address, 0x81);  // Display ON, Blinking OFF
}


/******************************************************************/
void ht16k33::tx(uint8_t slave_address, uint8_t data)
/*
short:         transmit single i2c bytes
inputs:        
outputs:    
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
  Wire.beginTransmission(slave_address);
  Wire.write(data);
  Wire.endTransmission();
}

/******************************************************************/
void ht16k33::tx(uint8_t slave_address, uint8_t *data, uint8_t lenght)
/*
short:      transmit i2c bytes
inputs:     
outputs:    
notes:      
Version :     DMK, Initial code
*******************************************************************/
{
  Wire.beginTransmission(slave_address);
  for(uint8_t idx; idx < lenght; idx++) {
    Wire.write(*data);
  }
  Wire.endTransmission();
}

/******************************************************************/
void ht16k33::animateCircle()
/*
short:    Start up animation
inputs:     
outputs:    
notes:      
Version : DMK, Initial code
*******************************************************************/
{
  setPixel(4+2, 3, ON); setPixel(16+2, 3, ON); display(); delay(50);
  setPixel(4+3, 2, ON); setPixel(16+3, 2, ON); display(); delay(50);
  setPixel(4+4, 2, ON); setPixel(16+4, 2, ON); display(); delay(50);
  setPixel(4+5, 3, ON); setPixel(16+5, 3, ON); display(); delay(50);
  setPixel(4+5, 4, ON); setPixel(16+5, 4, ON); display(); delay(50);
  setPixel(4+4, 5, ON); setPixel(16+4, 5, ON); display(); delay(50);
  setPixel(4+3, 5, ON); setPixel(16+3, 5, ON); display(); delay(50);
  setPixel(4+2, 4, ON); setPixel(16+2, 4, ON); display(); delay(50);

  setPixel(4+2, 3, OFF); setPixel(16+2, 3, OFF); display(); delay(50);
  setPixel(4+3, 2, OFF); setPixel(16+3, 2, OFF); display(); delay(50);
  setPixel(4+4, 2, OFF); setPixel(16+4, 2, OFF); display(); delay(50);
  setPixel(4+5, 3, OFF); setPixel(16+5, 3, OFF); display(); delay(50);
  setPixel(4+5, 4, OFF); setPixel(16+5, 4, OFF); display(); delay(50);
  setPixel(4+4, 5, OFF); setPixel(16+4, 5, OFF); display(); delay(50);
  setPixel(4+3, 5, OFF); setPixel(16+3, 5, OFF); display(); delay(50);
  setPixel(4+2, 4, OFF); setPixel(16+2, 4, OFF); display(); delay(50);
}

