/*-------------------------------------------------------------------------

mqtt to dot matrix leddisplay (HT16K33 based)

copyright 2018 dkroeske@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

-------------------------------------------------------------------------*/

#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <NeoPixelBus.h>
#include <DNSServer.h>
#include <WebServer.h>
#include <WiFiManager.h>
#include <SPI.h>
#include <Wire.h>
#include <PubSubClient.h>
#include <ESPmDNS.h>
#include <ArduinoJson.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <FS.h>
#include <SPIFFS.h>

#define DEBUG
//#define SECURE

#ifdef DEBUG
 #define DEBUG_PRINTF(format, ...) (Serial.printf(format, __VA_ARGS__))
#else
 #define DEBUG_PRINTF
#endif

#define RST_PIN 4 // IO4 on ESP32 PICO KIT V4
#define BL_PIN 14 // 
bool backlightAutoOff;          

#include "HT16K33.h"

// Neo pixel
#define NeoPixelCount   1     // Number of Neo pixels
#define NeoPixelPin     2     // Neo pixel IO2
#define colorSaturation 20    // Neo pixel brightness
#define colorFlashLevel 120   // Neo pixel brightness when flashing
NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> neoPixel(NeoPixelCount, NeoPixelPin);

// Neo pixel color presets
RgbColor red(colorSaturation, 0, 0);
RgbColor green(0, colorSaturation, 0);
RgbColor blue(0, 0, colorSaturation);
RgbColor white(colorSaturation);
RgbColor black(0);

// Local variables
WiFiManager wifiManager;

// Application configs struct. 
bool shouldSaveConfig;

#define MQTT_USERNAME_LENGTH       32
#define MQTT_PASSWORD_LENGTH       32
#define MQTT_ID_TOKEN_LENGTH       64
#define MQTT_TOPIC_STRING_LENGTH   64
#define MQTT_REMOTE_HOST_LENGTH   128
#define MQTT_REMOTE_PORT_LENGTH    10

typedef struct {
   char     mqtt_username[MQTT_USERNAME_LENGTH];
   char     mqtt_password[MQTT_PASSWORD_LENGTH];
   char     mqtt_id[MQTT_ID_TOKEN_LENGTH];
   char     mqtt_remote_host[MQTT_REMOTE_HOST_LENGTH];
   char     mqtt_remote_port[MQTT_REMOTE_HOST_LENGTH];
} APP_CONFIG_STRUCT;

APP_CONFIG_STRUCT app_config;

//Dot matrix LED displays, based on the Adafruit HT16K33 boards
ht16k33 matrix{};

// Prototype state functions. 
void fsm_start(void);
void fsm_s1(void);
void fsm_s2(void);
void fsm_stop(void);

// Define FSM (states, events)
typedef enum { EV_IDLY = 0, EV_STOP, EV_TRUE, EV_START } ENUM_EVENT;
typedef enum { STATE_START = 0, STATE_S1, STATE_S2, STATE_STOP } ENUM_STATE;

/* Define fsm transition */
typedef struct {
   void (*f)(void);
   ENUM_STATE nextState;
} STATE_TRANSITION_STRUCT;

// FSM definition (see statemachine diagram)
//
//       | EV_IDLY   EV_STOP  EV_TRUE  EV_START   
// -------------------------------------------------------------
// START | S1        STOP     -        -        Display text and wait before starting to scroll
// S1    | -         S2       S1       -        Scroll till end of message
// S2    | -         STOP     START    -        Display again or stop scrolling (done)
// STOP  | -         -        -        START    Wait for next text to display

STATE_TRANSITION_STRUCT fsm[4][4] = {
   { {fsm_s1, STATE_S1},  {fsm_stop, STATE_STOP}, {NULL, STATE_START},      {NULL, STATE_START} },     // State START
   { {NULL,   STATE_S1},  {fsm_s2, STATE_S2},     {fsm_s1, STATE_S1},       {NULL,   STATE_S1} },      // State S1
   { {NULL,   STATE_S2},  {fsm_stop, STATE_STOP}, {fsm_start, STATE_START}, {NULL, STATE_STOP} },      // State S2
   { {NULL,   STATE_STOP},{NULL, STATE_STOP},     {NULL,  STATE_STOP},      {fsm_start, STATE_START} } // State STOP
};

#define SCROLL_MAX_TEXT_LENGHT   64
#define SCROLL_INITIAL_DELAY_MS 750
#define SCROLL_SPEED_MS          40

typedef struct {
  char text[SCROLL_MAX_TEXT_LENGHT];
  uint16_t scrollCnt;
  uint16_t repeatCnt;
  uint16_t interval_ms;
  ENUM_STATE state;
  ENUM_EVENT event;
} TEXT_SCROLL_STRUCT;
TEXT_SCROLL_STRUCT textScroll = { "", 0, 0, 500, STATE_STOP, EV_TRUE };

#define TEXT_SCROLL_UPDATE_INTERVAL_MS 75
uint32_t text_scroll_cur=0, text_scroll_prev=0;

#ifdef DEBUG
// Default test.mosquitto certificate
const char* ca_cert= \
"-----BEGIN CERTIFICATE-----\n" \
"MIIC8DCCAlmgAwIBAgIJAOD63PlXjJi8MA0GCSqGSIb3DQEBBQUAMIGQMQswCQYD\n" \
"VQQGEwJHQjEXMBUGA1UECAwOVW5pdGVkIEtpbmdkb20xDjAMBgNVBAcMBURlcmJ5\n" \
"MRIwEAYDVQQKDAlNb3NxdWl0dG8xCzAJBgNVBAsMAkNBMRYwFAYDVQQDDA1tb3Nx\n" \
"dWl0dG8ub3JnMR8wHQYJKoZIhvcNAQkBFhByb2dlckBhdGNob28ub3JnMB4XDTEy\n" \
"MDYyOTIyMTE1OVoXDTIyMDYyNzIyMTE1OVowgZAxCzAJBgNVBAYTAkdCMRcwFQYD\n" \
"VQQIDA5Vbml0ZWQgS2luZ2RvbTEOMAwGA1UEBwwFRGVyYnkxEjAQBgNVBAoMCU1v\n" \
"c3F1aXR0bzELMAkGA1UECwwCQ0ExFjAUBgNVBAMMDW1vc3F1aXR0by5vcmcxHzAd\n" \
"BgkqhkiG9w0BCQEWEHJvZ2VyQGF0Y2hvby5vcmcwgZ8wDQYJKoZIhvcNAQEBBQAD\n" \
"gY0AMIGJAoGBAMYkLmX7SqOT/jJCZoQ1NWdCrr/pq47m3xxyXcI+FLEmwbE3R9vM\n" \
"rE6sRbP2S89pfrCt7iuITXPKycpUcIU0mtcT1OqxGBV2lb6RaOT2gC5pxyGaFJ+h\n" \
"A+GIbdYKO3JprPxSBoRponZJvDGEZuM3N7p3S/lRoi7G5wG5mvUmaE5RAgMBAAGj\n" \
"UDBOMB0GA1UdDgQWBBTad2QneVztIPQzRRGj6ZHKqJTv5jAfBgNVHSMEGDAWgBTa\n" \
"d2QneVztIPQzRRGj6ZHKqJTv5jAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUA\n" \
"A4GBAAqw1rK4NlRUCUBLhEFUQasjP7xfFqlVbE2cRy0Rs4o3KS0JwzQVBwG85xge\n" \
"REyPOFdGdhBY2P1FNRy0MDr6xr+D2ZOwxs63dG1nnAnWZg7qwoLgpZ4fESPD3PkA\n" \
"1ZgKJc2zbSQ9fCPxt2W3mdVav66c6fsb7els2W2Iz7gERJSX\n" \
"-----END CERTIFICATE-----\n";
#endif

#ifdef SECURE
  WiFiClientSecure wifiClient;
#else
  WiFiClient wifiClient;
#endif

// Only with some dummy values seems to work ... instead of mqttClient();
PubSubClient mqttClient("", 0, wifiClient);

// idle timer
#define IDLE_UPDATE_INTERVAL_SEC 1000 * 30
uint32_t idle_time_cur=0, idle_time_prev=0;

boolean idleTimerIsRunning = false;

void startIdleTimer() {
  idle_time_cur = idle_time_prev = millis();
  idleTimerIsRunning = true;
}

void stopIdleTimer() {
  idleTimerIsRunning = false;
}

boolean checkIdleTimer() {
  boolean retval = false;
  
  if( idleTimerIsRunning ) {
    idle_time_cur = millis();
    uint32_t idle_time_elapsed = idle_time_cur - idle_time_prev;
    if( idle_time_elapsed >= IDLE_UPDATE_INTERVAL_SEC ) {
      retval = true;
    }
  }  
  return retval;
}

// Some LOGO displayed at boot
static const uint8_t PROGMEM ilovedots [] = {
0x00, 0x0f, 0xf0, 0x3c, 0x3c, 0x0f, 0xe0, 0x03, 0xf0, 0x3f, 0xff, 0x07, 0xe0, 0x00, 0x00, 0x0f, 
0xf0, 0x7e, 0x7e, 0x0f, 0xf8, 0x07, 0xf8, 0x3f, 0xff, 0x1f, 0xf0, 0x00, 0x00, 0x0f, 0xf0, 0xff, 
0xff, 0x0f, 0xfc, 0x0f, 0xfc, 0x3f, 0xff, 0x1f, 0xf0, 0x00, 0x00, 0x03, 0xc1, 0xe7, 0xe7, 0x8e, 
0x3e, 0x1f, 0x3e, 0x3f, 0xff, 0x3c, 0x30, 0x00, 0x00, 0x03, 0xc1, 0xc3, 0xc3, 0x8e, 0x1e, 0x1e, 
0x1e, 0x01, 0xe0, 0x3c, 0x10, 0x00, 0x00, 0x03, 0xc1, 0xc1, 0x83, 0x8e, 0x0f, 0x3c, 0x0f, 0x01, 
0xe0, 0x3e, 0x00, 0x00, 0x00, 0x03, 0xc1, 0xc0, 0x03, 0x8e, 0x0f, 0x3c, 0x0f, 0x01, 0xe0, 0x3f, 
0xc0, 0x00, 0x00, 0x03, 0xc1, 0xe0, 0x07, 0x8e, 0x0f, 0x3c, 0x0f, 0x01, 0xe0, 0x1f, 0xf0, 0x00, 
0x00, 0x03, 0xc0, 0xe0, 0x07, 0x0e, 0x0f, 0x3c, 0x0f, 0x01, 0xe0, 0x0f, 0xf0, 0x00, 0x00, 0x03, 
0xc0, 0xf0, 0x0f, 0x0e, 0x0f, 0x3c, 0x0f, 0x01, 0xe0, 0x03, 0xf8, 0x00, 0x00, 0x03, 0xc0, 0x78, 
0x1e, 0x0e, 0x0f, 0x3c, 0x0f, 0x01, 0xe0, 0x00, 0xf8, 0x00, 0x00, 0x03, 0xc0, 0x3c, 0x3c, 0x0e, 
0x1e, 0x1e, 0x1e, 0x01, 0xe0, 0x20, 0x78, 0x00, 0x00, 0x03, 0xc0, 0x3e, 0x7c, 0x0e, 0x3e, 0x1f, 
0x3e, 0x01, 0xe0, 0x30, 0x78, 0x00, 0x00, 0x0f, 0xf0, 0x0f, 0xf0, 0x0f, 0xfc, 0x0f, 0xfc, 0x01, 
0xe0, 0x3f, 0xf0, 0x00, 0x00, 0x0f, 0xf0, 0x07, 0xe0, 0x0f, 0xf8, 0x07, 0xf8, 0x01, 0xe0, 0x3f, 
0xe0, 0x00, 0x00, 0x0f, 0xf0, 0x01, 0x80, 0x0f, 0xe0, 0x03, 0xf0, 0x01, 0xe0, 0x0f, 0xc0, 0x00
};

// mqtt topic strings: flipdot/<uid>/msg and flipdot/<uid>/msg
char mqtt_topic_raw[64];
char mqtt_topic_msg[64];

//
//
//
void setup() {

  // Init Serial port
  Serial.begin(115200, SERIAL_8N1);
  Serial.printf("\n\r... in debug mode ...\n\r");

  // Test is SPIFFS is already formatted
  if(!SPIFFS.begin()){
    SPIFFS.begin(true);
    Serial.printf("Format SPIFFS ...\n\r");
    ESP.restart();
  }

  // Define I/O pins
  pinMode(RST_PIN, INPUT);

  // Setup unique mqtt id and mqtt topic string
  sprintf(app_config.mqtt_id,"HFD_112x16_01_%08X",ESP.getEfuseMac()); 
  sprintf(mqtt_topic_raw,"DotMessage/HFD-%08X/raw",ESP.getEfuseMac());
  sprintf(mqtt_topic_msg,"DotMessage/HFD-%08X/msg",ESP.getEfuseMac());
  
  // Init with red led
  smartLedInit(red);
  for(int idx = 0; idx < 5; idx++ ) {
    smartLedFlash(colorFlashLevel);
    delay(100);
  }
  
  // Perform factory when reset
  // is pressed during powerup
  if( 0 == digitalRead(RST_PIN) ) {
    DEBUG_PRINTF("%s:RST pushbutton pressed, reset WiFiManager and appConfig settings\n", __FUNCTION__);
    //wifiManager.resetSettings(); // Bugs 
    deleteAppConfig(SPIFFS, "/config.json");

    // Bugfix:
    if( wifiManager.autoConnect("LEDMatrix Config") ) {
      WiFi.disconnect(true);
      delay(1000);
      ESP.restart();
    }
    DEBUG_PRINTF("%s:ssid erased\n", __FUNCTION__);
    
    while(0 == digitalRead(RST_PIN)) {
       smartLedFlash(colorFlashLevel);
       DEBUG_PRINTF("%s:Wait for user to release RST pin\n", __FUNCTION__);
       delay(500);
    }
    ESP.restart();
  }

  // Read config file or generate default
  if( !readAppConfig(SPIFFS, "/config.json", &app_config) ) {
    strcpy(app_config.mqtt_username, "");
    strcpy(app_config.mqtt_password, "");
    strcpy(app_config.mqtt_remote_host, "test.mosquitto.org");
    strcpy(app_config.mqtt_remote_port, "1883");
    writeAppConfig(SPIFFS, "/config.json", &app_config);
  }

  // Setup WiFiManager
  smartLedShowColor(blue);
  wifiManager.setMinimumSignalQuality(20);
  wifiManager.setTimeout(300);
  wifiManager.setSaveConfigCallback(saveConfigCallback);
  wifiManager.setDebugOutput(false);
  shouldSaveConfig = false;

  // Adds some parameters to the default webpage
  WiFiManagerParameter wmp_text("<br/>MQTT setting:</br>");
  wifiManager.addParameter(&wmp_text);
  WiFiManagerParameter custom_mqtt_username("mqtt_username", "Username", app_config.mqtt_username, MQTT_USERNAME_LENGTH);
  WiFiManagerParameter custom_mqtt_password("mqtt_password", "Password", app_config.mqtt_password, MQTT_PASSWORD_LENGTH);
  WiFiManagerParameter custom_mqtt_remote_host("mqtt_remote_host", "Host", app_config.mqtt_remote_host, MQTT_REMOTE_HOST_LENGTH);
  WiFiManagerParameter custom_mqtt_remote_port("mqtt_port", "Port", app_config.mqtt_remote_port, MQTT_REMOTE_PORT_LENGTH);
  wifiManager.addParameter(&custom_mqtt_username);
  wifiManager.addParameter(&custom_mqtt_password);
  wifiManager.addParameter(&custom_mqtt_remote_host);
  wifiManager.addParameter(&custom_mqtt_remote_port);

  // Add the unit ID to the webpage
  char fd_str[128];
  sprintf(fd_str, "<p>Your LED dotmatrix signature: <b>%08X</b> (You will need this later in the app)", ESP.getEfuseMac());
  WiFiManagerParameter mqqt_topic_text(fd_str);
  wifiManager.addParameter(&mqqt_topic_text);

  // Start WiFiManager ...
  if( !wifiManager.autoConnect("Ledmatrix HT16K33 Config")) {
    delay(1000);
    ESP.restart();
  }

  //
  // Update config if needed
  //
  if(shouldSaveConfig) {
    strcpy(app_config.mqtt_username, custom_mqtt_username.getValue());
    strcpy(app_config.mqtt_password, custom_mqtt_password.getValue());
    strcpy(app_config.mqtt_remote_host, custom_mqtt_remote_host.getValue());
    strcpy(app_config.mqtt_remote_port, custom_mqtt_remote_port.getValue());
    writeAppConfig(SPIFFS, "/config.json", &app_config);
  }

  // Setup OTA
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  // ArduinoOTA.setHostname("myesp8266");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  //
  DEBUG_PRINTF("%s:Setting MDNS service to 'Hanover_112x16_flipdot_v10'\n",__FUNCTION__);
  if( !MDNS.begin("Hanover_112x16_flipdot_v10") ) {
  } else {
    MDNS.addService("Hanover_112x16_flipdot_v10", "tcp", 10000);
  }

#ifdef SECURE
  DEBUG_PRINTF("%s:Setting CERT file\n",__FUNCTION__);
  wifiClient.setCACert(ca_cert);
#endif

  // Start idle timer
  startIdleTimer();

  // Start OverTheAir update services
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    DEBUG_PRINTF("%s:Start updating %s ...\n",__FUNCTION__, type.c_str());
  });
  
  ArduinoOTA.onEnd([]() {
    DEBUG_PRINTF("\n%s:End",__FUNCTION__);
  });
  
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    DEBUG_PRINTF("Progress: %u%%\r", (progress / (total / 100)));
  });
    
  ArduinoOTA.onError([](ota_error_t error) {
    DEBUG_PRINTF("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) DEBUG_PRINTF("%s:Auth Failed\n",__FUNCTION__);
    else if (error == OTA_BEGIN_ERROR) DEBUG_PRINTF("%s:Begin Failed",__FUNCTION__);
    else if (error == OTA_CONNECT_ERROR) DEBUG_PRINTF("%s:Connect Failed",__FUNCTION__);
    else if (error == OTA_RECEIVE_ERROR) DEBUG_PRINTF("%s:Receive Failed",__FUNCTION__);
    else if (error == OTA_END_ERROR) DEBUG_PRINTF("%s:End Failed",__FUNCTION__);
  });

  ArduinoOTA.begin();

  // Debug print
  DEBUG_PRINTF("%s:OAT ready\n", __FUNCTION__);
  DEBUG_PRINTF("%s:mqtt_username: %s\n", __FUNCTION__, app_config.mqtt_username);
  DEBUG_PRINTF("%s:mqtt_password: %s\n", __FUNCTION__, app_config.mqtt_password);
  DEBUG_PRINTF("%s:mqtt_id: %s\n", __FUNCTION__, app_config.mqtt_id);
  DEBUG_PRINTF("%s:mqtt_topic RAW: %s\n", __FUNCTION__, mqtt_topic_raw);
  DEBUG_PRINTF("%s:mqtt_topic MSG: %s\n", __FUNCTION__, mqtt_topic_msg);
  DEBUG_PRINTF("%s:mqtt_remote_host: %s\n", __FUNCTION__, app_config.mqtt_remote_host);
  DEBUG_PRINTF("%s:mqtt_remote_port: %s\n", __FUNCTION__, app_config.mqtt_remote_port);

  // Active ledmatrix and display ip + signature info on dotmatrix
  matrix.clear();
  matrix.animateCircle();
  delay(500);
  char str[64];
  sprintf(str, "HFD-%08X", ESP.getEfuseMac()); 
  Serial.println(str); 
  displayText(str);
}

//
// MQTT callback
//
#define DISPLAY_WIDTH 112
#define DISPLAY_HEIGHT 16

void mqtt_callback(char* topic, byte* payload, unsigned int length) {

  // 
  // Raw Message (pixels)
  // 
  if( 0 == strcmp(topic, mqtt_topic_raw) ) {

    // Reset timer
    startIdleTimer();

    //
    uint8_t buf[ DISPLAY_WIDTH * DISPLAY_HEIGHT/8 ];
    uint8_t *pbuf = buf;
    
    DynamicJsonBuffer jsonBuffer(2048);
    //StaticJsonBuffer<2048> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(payload);
    if( root.success() ) {
      JsonArray& pixels = root["pixels"]; 
      for( JsonArray::iterator it = pixels.begin(); it!=pixels.end(); ++it) {
        *pbuf++ = it->as<uint8_t>();
      }
    }

    // Flash the pixel
    smartLedFlash(colorFlashLevel);
  }

  // 
  // Normal Text
  // 
  if( 0 == strcmp(topic, mqtt_topic_msg) ) {

    // Reset timer
    startIdleTimer();

    // Parse message and display
    DynamicJsonBuffer jsonBuffer(2048);
    JsonObject& root = jsonBuffer.parseObject(payload);
    if( root.success() ) {

      // Test for message and flipdot message
      JsonVariant msg = root["message"];
      if(msg.success()) {
        displayText(msg.as<const char*>());
        
        // Flash the pixel
        smartLedFlash(colorFlashLevel);
      }
    }
  }
}

//
//
//
void loop() {

  // Check for IP connection
  if( WiFi.status() == WL_CONNECTED) {

    // Check for OTA firmware updates
    ArduinoOTA.handle();

    // Handle mqtt
    if( !mqttClient.connected() ) {
      mqtt_connect();
      delay(250);
    } else {
      // Handle MQTT loop
      mqttClient.loop();
    }
  } 

  // 
  // Handle display scroll
  //
  text_scroll_cur = millis();
  uint32_t text_scroll_elapsed = text_scroll_cur - text_scroll_prev;
  if( text_scroll_elapsed >= TEXT_SCROLL_UPDATE_INTERVAL_MS ) {
    text_scroll_prev = text_scroll_cur; 
    handleTextScrollEvent();
  }
}

/******************************************************************/
/*
 * FSM section
 */
/******************************************************************/
 
/******************************************************************/
void raiseEvent(ENUM_EVENT newEvent)
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
   textScroll.event = newEvent;
}


/******************************************************************/
void handleTextScrollEvent()
/* 
short:         
inputs:        
outputs: 
notes:         
Version:  DMK, Initial code
*******************************************************************/
{
   //DEBUG_PRINTF("%s: STATE: %s, EVENT: %s\n", __FUNCTION__, state(textScroll.state), event(textScroll.event) );

   ENUM_EVENT prev = textScroll.event;
      
   // Call State function
   if( fsm[textScroll.state][textScroll.event].f != NULL) {
      fsm[textScroll.state][textScroll.event].f() ;
   } 
   
   // Set new state
   textScroll.state = fsm[textScroll.state][prev].nextState;
}


/******************************************************************/
void fsm_start(void)
/* 
short:         
inputs:        
outputs: 
notes:         
Version:  DMK, Initial code
*******************************************************************/
{
  DEBUG_PRINTF("%s:\n", __FUNCTION__);
  
  matrix.clear();
  matrix.drawText(textScroll.text, 0, 0);
  textScroll.scrollCnt = 0;
  matrix.display(0,0);

  textScroll.interval_ms = SCROLL_INITIAL_DELAY_MS;
  raiseEvent(EV_IDLY);
}

/******************************************************************/
void fsm_s1(void)
/* 
short:         
inputs:        
outputs: 
notes:         
Version:  DMK, Initial code
*******************************************************************/
{  
  //DEBUG_PRINTF("%s:\n", __FUNCTION__);

  textScroll.interval_ms = SCROLL_SPEED_MS;

  if( textScroll.scrollCnt < (strlen(textScroll.text) * 8 + 24) ) {
    matrix.display(textScroll.scrollCnt++, 0);
    raiseEvent(EV_TRUE);
  } else {
    raiseEvent(EV_STOP);
  }
}

/******************************************************************/
void fsm_s2(void)
/* 
short:         
inputs:        
outputs: 
notes:         
Version:  DMK, Initial code
*******************************************************************/
{
  Serial.printf("%s:\n", __FUNCTION__);
  switch( textScroll.repeatCnt ) {
    case 0: 
      raiseEvent(EV_STOP); 
      break;
    default:
      textScroll.repeatCnt--;
      raiseEvent(EV_START);
      break;
  }
}

/******************************************************************/
void fsm_stop(void)/* 
short:         
inputs:        
outputs: 
notes:         
Version:  DMK, Initial code
*******************************************************************/
{
  DEBUG_PRINTF("%s:\n", __FUNCTION__);
  raiseEvent(EV_TRUE);
}

/******************************************************************/
void displayText(const char *msg)
/* 
short:      
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
  matrix.clear();
  matrix.animateCircle();
  delay(250);
  
  // Set scroll text
  strcpy(textScroll.text, msg);
  textScroll.repeatCnt = 1;

  // Reset scroll fsm and start scrolling
  textScroll.state = STATE_STOP;
  raiseEvent(EV_START);  
}

/******************************************************************/
const char * state(ENUM_STATE state)
/* 
short:         
inputs:        
outputs: 
notes:         
Version:  DMK, Initial code
*******************************************************************/
{
  switch(state) {
    case 0: return("STATE_START");
    case 1: return("STATE_S1");
    case 2: return("STATE_S2");
    case 3: return("STATE_STOP");
    default: return("UNKNOWN-STATE");  
  }
}

/******************************************************************/
const char * event(ENUM_EVENT event)
/* 
short:         
inputs:        
outputs: 
notes:         
Version:  DMK, Initial code
*******************************************************************/
{
  switch(event) {
    case 0: return("EV_IDLY");
    case 1: return("EV_STOP");
    case 2: return("EV_TRUE");
    case 3: return("EV_START");
    default: return("UNKNOWN-event");  
  }
}

/******************************************************************/
void mqtt_connect() 
/* 
short:      Connect to MQTT server UNSECURE
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
  char *host = app_config.mqtt_remote_host;
  int port = atoi(app_config.mqtt_remote_port);
  
  mqttClient.setClient(wifiClient);
  mqttClient.setServer(host, port);
  if(mqttClient.connect(app_config.mqtt_id)){

    // Subscribe to ../raw and ../msg
    mqttClient.subscribe(mqtt_topic_raw);
    mqttClient.subscribe(mqtt_topic_msg);

    // Set callback
    mqttClient.setCallback(mqtt_callback);
    DEBUG_PRINTF("%s: MQTT connected to %s:%d\n", __FUNCTION__, host, port);
    
    smartLedInit(green);

  } else {    
    DEBUG_PRINTF("%s: MQTT connection ERROR (%s:%d)\n", __FUNCTION__, host, port);
    smartLedInit(red);
  }
}


/******************************************************************/
/*
 * Smart LED section
 */
/******************************************************************/
 
/******************************************************************/
void smartLedShowColor(RgbColor color)
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
  // Set new color
  neoPixel.SetPixelColor(0, color);
  neoPixel.Show(); 
}

/******************************************************************/
void smartLedFlash(uint8_t level)
/* 
short:      Flash current color         
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
  RgbColor old = neoPixel.GetPixelColor(0);
  RgbColor flash = old;
  if(0!=old.R) flash.R = level;
  if(0!=old.G) flash.G = level;
  if(0!=old.B) flash.B = level;
  neoPixel.SetPixelColor(0, flash);
  neoPixel.Show();
  delay(50);
  neoPixel.SetPixelColor(0, old);
  neoPixel.Show();
}

/******************************************************************/
void smartLedInit(RgbColor color)
/* 
short:      Init         
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
  neoPixel.Begin();
  smartLedShowColor(color);
}

/******************************************************************/
/*
 * Application signature and config
 */
/******************************************************************/

/******************************************************************/
void saveConfigCallback () 
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
  shouldSaveConfig = true;
}

/******************************************************************/
bool readAppConfig(fs::FS &fs, const char *path, APP_CONFIG_STRUCT *app_config) 
/* 
short:         Read config struct from flash
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
  bool retval = false;

  File file = fs.open(path);
  
  if( !file || file.isDirectory() )
  {
    DEBUG_PRINTF("%s: Failed to open (%s)\n", __FUNCTION__, path);
    return retval;
  }

  if( file ) {
    size_t size = file.size();
    std::unique_ptr<char[]> buf(new char[size]);
    
    file.readBytes(buf.get(), size);
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.parseObject(buf.get());
    if( json.success() ) {
       strcpy(app_config->mqtt_username, json["MQTT_USERNAME"]);
       strcpy(app_config->mqtt_password, json["MQTT_PASSWORD"]);
       strcpy(app_config->mqtt_remote_host, json["MQTT_HOST"]);
       strcpy(app_config->mqtt_remote_port, json["MQTT_PORT"]);
       retval = true;
    }
  } 
  
  return retval;
}

/******************************************************************/
bool writeAppConfig(fs::FS &fs, const char *path, APP_CONFIG_STRUCT *app_config) 
/* 
short:         Write config to FFS
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
  bool retval = false;
  
  File file = fs.open(path, FILE_WRITE);
  if(!file){
      DEBUG_PRINTF("%s: failed to open file (%s) for writing\n", __FUNCTION__, path);
      return retval;
  }

  // Create new and store settings
  DynamicJsonBuffer jsonBuffer;
  JsonObject& json = jsonBuffer.createObject();
  json["MQTT_USERNAME"] = app_config->mqtt_username;
  json["MQTT_PASSWORD"] = app_config->mqtt_password;
  json["MQTT_HOST"] = app_config->mqtt_remote_host;
  json["MQTT_PORT"] = app_config->mqtt_remote_port;

  char buf[1024];
  json.printTo(buf);
  
  if(file.print(buf)){
      retval = true;
  } else {
      DEBUG_PRINTF("%s: failed to write to (%s)\n", __FUNCTION__, path);
  } 
  
  return retval;
}

/******************************************************************/
void deleteAppConfig(fs::FS &fs, const char * path) 
/* 
short:         Delete confif from SPIFFS
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
  if(!fs.remove(path)){
    DEBUG_PRINTF("%s: failed to delete (%s)\n", __FUNCTION__, path);
  }
}

