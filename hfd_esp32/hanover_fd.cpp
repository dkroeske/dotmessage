/*-------------------------------------------------------------------------

esp8266 Driver for Hanover flipdot displays.

copyright 2018 dkroeske@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

-------------------------------------------------------------------------*/

#include <stdlib.h>
#include <malloc.h>
#include <pgmspace.h>
#include "hanover_fd.h"
#include "fonts.h"
#include "HardwareSerial.h"

extern HardwareSerial Serial1;

#define DISPLAY_ADDR 0
#define swap(a, b) { int16_t t = a; a = b; b = t; }

//
uint8_t display_width;
uint8_t display_height;
uint8_t display_address;

uint8_t rotation = 0;

#define HANOVER_DISPLAY_HEADER_SIZE 5
#define HANOVER_DISPLAY_FOOTER_SIZE 3
#define HANOVER_DISPLAY_DATA_SIZE (2 * 112 * 16 / 8)

// rs485 message definition to Hanover display
typedef struct {
  uint8_t header[HANOVER_DISPLAY_HEADER_SIZE];
//  uint8_t data[HANOVER_DISPLAY_DATA_SIZE];
  uint8_t *data;
  uint8_t footer[HANOVER_DISPLAY_FOOTER_SIZE];
} HANOVER_DISPLAY_STRUCT;

HANOVER_DISPLAY_STRUCT hds = {
    {0x02, 0x31, 0x31, 0x00, 0x00}, // [stx]['1'][address][display_size][display_size]
    NULL, 
    {0x03, 0x00, 0x00}              // [eot][crc][crc]
};

// Cursor definition
typedef struct {
  uint8_t x;
  uint8_t y;
} CURSOR_STRUCT;

CURSOR_STRUCT cursor = {0,0};

// Internal display buffer is as an lineare array of uint8_t. Every 
// element maps to 8 pixels (rows) on the display. 
// After [DISPLAY_WIDTH] the next 8 rows are mapped.
// buffer runtime created in the constructor with size = width * height / 8
// All displayfunctions are performed on the buffer. Display() converts
// and writes 'buffer' to the Hanover display
uint8_t *buffer;

/******************************************************************/
HanoverFlipDot::HanoverFlipDot()
/* 
short:      Default constructor
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
  // Init flipdot
  HanoverFlipDot(112, 16, 0);
  rotation = 0;
}

/******************************************************************/
HanoverFlipDot::HanoverFlipDot(uint8_t width, uint8_t height, uint8_t address ) 
/* 
short:      constructor
inputs:     width and height of Hanover display in pixels. Address equals
            display rs485 address   
outputs:  
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
  display_width = width;
  display_height = height;
  display_address = address;
  rotation = 0;

  // Construct 'buffer' on heap
  buffer = (uint8_t *)malloc( (display_width * display_height/8) * sizeof(uint8_t) );
  if( buffer != NULL ) {
    memset(buffer, 0x00, (display_width * display_height/8)); 
  } else {
    //Serial1.println("Malloc display buffer Error");
  }

  // Construct rs485 datablock
  hds.data = (uint8_t *)malloc( ( 2 * display_width * display_height/8) * sizeof(uint8_t) );
  if( hds.data == NULL ) {
    //Serial1.println("Malloc hds.data ERROR");
  }
}

/******************************************************************/
void HanoverFlipDot::directDrawBuffer( const uint8_t *data ) {
/* 
short:      Copy data direct in buffer
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
  memcpy(buffer, data, (display_width * display_height/8));
}

/******************************************************************/
void HanoverFlipDot::setFont(const char *fontName)
/* 
short:      Draw pixel in buffer
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
{
   
}


/******************************************************************/
void HanoverFlipDot::drawPixel(uint8_t x, uint8_t y, uint8_t color)
/* 
short:      Draw pixel in buffer
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
{
  if ((x < 0) || (x >= display_width) || (y < 0) || (y >= display_height)) {
    return;
  }

  // Handle x,y depending on rotation
  switch (rotation) {
    case 1:
      swap(x, y);
      x = display_width - x - 1;
      break;
    case 2:
      x = display_width - x - 1;
      y = display_height - y - 1;
      break;
    case 3:
      swap(x, y);
      y = display_height - y - 1;
      break;
  }

  // x indicates colomn
  switch (color)
  {
    case YELLOW:  *(buffer + x + (y/8)*display_width) |=  (1 << (y&7)); break;
    case BLACK:   *(buffer + x + (y/8)*display_width) &= ~(1 << (y&7)); break;
    case INVERSE: *(buffer + x + (y/8)*display_width) ^=  (1 << (y&7)); break;
  }
  yield();
}

///******************************************************************/
//void HanoverFlipDot::drawChar(uint8_t x, uint8_t y, char ch)
///* 
//short:      Draw character in buffer
//inputs:     
//outputs:  
//notes:      
//Version :   DMK, Initial code
//*******************************************************************/
//{
//  for(uint8_t idx = 0; idx < 8; idx++){
//    uint8_t b = pgm_read_byte(unscii[ch] + idx);
//    for(uint8_t idy = 0; idy < 8; idy++) {
//      if( b & (1<<idy) ) {
//        drawPixel(idx + x, idy + y, YELLOW);
//      } else {
//        drawPixel(idx + x, idy + y, BLACK);
//      }
//    }
//  }
//}

/******************************************************************/
void HanoverFlipDot::drawChar(uint8_t x, uint8_t y, char ch)
/* 
short:      Draw character in buffer
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
{
  
  for(uint8_t idx = 0; idx < 8; idx++){
    
    uint8_t b = pgm_read_byte( (unscii.fontMap + (ch * 8 + idx)) );

    for(uint8_t idy = 0; idy < 8; idy++) {
      if( b & (1<<idy) ) {
        drawPixel(idx + x, idy + y, YELLOW);
      } else {
        drawPixel(idx + x, idy + y, BLACK);
      }
    }
  }
}

/******************************************************************/
void HanoverFlipDot::drawText(uint8_t x, uint8_t y, const char *c_str)
/* 
short:      Draw string in buffer
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
{
  while(*c_str){
    if( *c_str == '\n' ) {
      y += 8;
      x = 0;
    } else {
      drawChar(x,y,(char)*c_str);
      x+=8;
    }
    c_str++;
  }
}

/******************************************************************/
void HanoverFlipDot::setCursor(uint8_t x, uint8_t y)
/* 
short:      
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
{
  cursor.x = x; 
  cursor.y = y;
}

/******************************************************************/
void HanoverFlipDot::println(const char *c_str)
/* 
short:      Draw text on current cursor position
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
{
  drawText(cursor.x, cursor.y, c_str);
}

/******************************************************************/
void HanoverFlipDot::drawBitmap(uint8_t x, uint8_t y, const uint8_t *bitmap, uint8_t width, uint8_t height)
/* 
short:      Draw bitmnap in display
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
{  
}

/******************************************************************/
void HanoverFlipDot::invert(void)
/* 
short:      Invert all pixel in buffer
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
{
  for( uint8_t y = 0; y < (display_width * display_height / 8); y++ ) {
    (*(buffer+y))^=0xFF;
    yield();
  } 
}

/******************************************************************/
void HanoverFlipDot::clr(void)
/* 
short:      Set all the pixel off in buffer
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
{
  memset(buffer, 0x00, (display_width * display_height/8));
}

/******************************************************************/
void HanoverFlipDot::set(void)
/* 
short:      Set all the pixel on in buffer
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
{
  memset(buffer, 0xFF, (display_width * display_height/8));
}

/******************************************************************/
void HanoverFlipDot::setRotation(uint8_t rotation)
/* 
short:      Set all the pixel on in buffer
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
{
  ::rotation = rotation;
}

/******************************************************************/
uint8_t HanoverFlipDot::getWidthInPixels(void)
/* 
short:      Set all pixels on in buffer
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
{
  return display_width;  
}

/******************************************************************/
uint8_t HanoverFlipDot::getHeightInPixels(void)
/* 
short:      Set all pixels on in buffer
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
{
  return display_height;  
}


/******************************************************************/
void HanoverFlipDot::display(void)
/* 
short:      Write buffer to Hanover flipdot. Let the dots flip!
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
{

  // fill header
  hds.header[2] = toAscii(display_address + 1);
  uint8_t dSize = display_width * display_height / 8;
  hds.header[3] = toAscii((dSize >> 4) & 0x0F);
  hds.header[4] = toAscii(dSize & 0x0F);

  // Datablock. Every nibble in colomn as ascii value
  for( uint8_t idx = 0; idx < display_width; idx++) {
    for( uint8_t idy = 0; idy < display_height / 8; idy++) {
      // Get byte
      uint8_t b = buffer[idy * display_width + idx];
      
      // Split into ascii
      uint8_t a0 = toAscii((b >> 4) & 0x0F);
      uint8_t a1 = toAscii(b & 0x0F);
      
      // Calculate index in rs285 data buffer
      uint16_t i0 = (2 * idx * display_height/8) + (2 * idy + 0);
      uint16_t i1 = (2 * idx * display_height/8) + (2 * idy + 1);

      // Store ascii value in rs485 data buffer
      hds.data[i0] = a0;
      hds.data[i1] = a1;

      // Prevent WD reset
      yield();
    }
  }
    
  // Calculate crc
  uint8_t crc = 0;
  for( int8_t t = 1; t < 5; t++) crc += hds.header[t];
  for( int16_t t = 0; t < 2 * display_width * display_height / 8; t++) crc += hds.data[t];
  crc += hds.footer[0];
  crc &= 0xFF;
  crc ^= 255;
  crc += 1;
  hds.footer[1] = toAscii((crc >> 4) & 0x0F);
  hds.footer[2] = toAscii(crc & 0x0F);

  // Transmit header to hanover display
  for( uint8_t idx = 0; idx < 5 ; idx ++ ) {
    Serial1.write(hds.header[idx]);
  }

  // Transmit datablock
  for( uint16_t idx = 0; idx < 2 * display_width * display_height / 8; idx ++ ) {
    Serial1.write(hds.data[idx]);
    yield();
  } 

  // Transmit footer to hanover display
  for( uint8_t idx = 0; idx < 3 ; idx ++ ) {
    Serial1.write(hds.footer[idx]);
  }

//  // Debug
//  Serial1.printf("\nHanover display HEADER: \n");
//  for( uint8_t idx = 0; idx < 5 ; idx ++ ) {
//    Serial1.printf("0x%.2X ", hds.header[idx]);
//  }
//  
//  Serial1.printf("\nHanover display DATA: \n");
//  for( uint16_t idx = 0; idx < 2 * DISPLAY_WIDTH * DISPLAY_HEIGHT / 8; idx ++ ) {
//    Serial1.printf("0x%.2X ", hds.data[idx]);
//    if( idx % 16 == 0 ) {
//      Serial1.printf("\n");
//    }
//    yield();
//  }
//
//  Serial1.printf("\nHanover display FOOTER: \n");
//  for( uint8_t idx = 0; idx < 3 ; idx ++ ) {
//    Serial1.printf("0x%.2X ", hds.footer[idx]);
//  }
}

/******************************************************************/
uint8_t HanoverFlipDot::toAscii(uint8_t val)
/* 
short:      value to ascii-value. Need in the rs485 Hanover comm
inputs:     
outputs:  
notes:      
Version :   DMK, Initial code
*******************************************************************/
{
  if( val >= 10 ) {
    return val - 10 + 'A';
  }
  else {
    return val + '0';
  }
}

