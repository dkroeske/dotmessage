/*-------------------------------------------------------------------------

esp8266 Driver for Hanover flipdot displays.

copyright 2018 dkroeske@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

-------------------------------------------------------------------------*/ 

#ifndef _HANOVER_FLIPDOT_H
#define _HANOVER_FLIPDOT_H

#include "Arduino.h"

// Dot colors
#define BLACK   0
#define YELLOW  1
#define INVERSE 2

class HanoverFlipDot {
 public:

    HanoverFlipDot();  
    HanoverFlipDot(uint8_t width, uint8_t height, uint8_t address );  

    void clr(void);
    void set(void);
    void invert(void);
    void setCursor(uint8_t x, uint8_t y);

    void setRotation(uint8_t rotation);
    uint8_t getWidthInPixels(void);
    uint8_t getHeightInPixels(void);

    void directDrawBuffer( const uint8_t *data );
    void drawPixel(uint8_t x, uint8_t y, uint8_t color);
    void drawBitmap(uint8_t x, uint8_t y, const uint8_t *bitmap, uint8_t width, uint8_t height);
    void drawChar(uint8_t x, uint8_t y, char ch);
    void drawText(uint8_t x, uint8_t y, const char *c_str);
    void println(const char *c_str);
    void setFont(const char *fontName);
    void display();

  private:
    uint8_t toAscii(uint8_t val);
};

#endif
