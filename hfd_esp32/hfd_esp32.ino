/*-------------------------------------------------------------------------

Flipdot mqtt to Hanover flipdot display (or whatever display). 

copyright 2018 dkroeske@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

-------------------------------------------------------------------------*/

#include <WiFi.h>
#include <NeoPixelBus.h>
#include <DNSServer.h>
#include <WebServer.h>
#include <WiFiManager.h>
#include <SPI.h>
#include <Wire.h>
#include <PubSubClient.h>
#include <ESPmDNS.h>
#include <ArduinoJson.h>
#include <time.h>
#include <simpleDSTadjust.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <FS.h>
#include <SPIFFS.h>

#define DEBUG

#ifdef DEBUG
 #define DEBUG_PRINTF(format, ...) (Serial.printf(format, __VA_ARGS__))
#else
 #define DEBUG_PRINTF
#endif

#define RST_PIN 4 // IO4 on ESP32 PICO KIT V4
#define BL_PIN 14 // 
bool backlightAutoOff;          

#include "hanover_fd.h"

// Neo pixel
#define NeoPixelCount   1     // Number of Neo pixels
#define NeoPixelPin     2     // Neo pixel IO2
#define colorSaturation 20    // Neo pixel brightness
#define colorFlashLevel 120   // Neo pixel brightness when flashing
NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> neoPixel(NeoPixelCount, NeoPixelPin);

// Neo pixel color presets
RgbColor red(colorSaturation, 0, 0);
RgbColor green(0, colorSaturation, 0);
RgbColor blue(0, 0, colorSaturation);
RgbColor white(colorSaturation);
RgbColor black(0);

// Local variables
WiFiManager wifiManager;

// Application configs struct. 
bool shouldSaveConfig;

#define MQTT_USERNAME_LENGTH       32
#define MQTT_PASSWORD_LENGTH       32
#define MQTT_ID_TOKEN_LENGTH       64
#define MQTT_TOPIC_STRING_LENGTH   64
#define MQTT_REMOTE_HOST_LENGTH   128
#define MQTT_REMOTE_PORT_LENGTH    10

typedef struct {
   char     mqtt_username[MQTT_USERNAME_LENGTH];
   char     mqtt_password[MQTT_PASSWORD_LENGTH];
   char     mqtt_id[MQTT_ID_TOKEN_LENGTH];
   char     mqtt_remote_host[MQTT_REMOTE_HOST_LENGTH];
   char     mqtt_remote_port[MQTT_REMOTE_HOST_LENGTH];
} APP_CONFIG_STRUCT;

APP_CONFIG_STRUCT app_config;

HanoverFlipDot flipdot = HanoverFlipDot();

WiFiClient wifiClient;

// Only with some dummy values seems to work ... instead of mqttClient();
PubSubClient mqttClient("", 0, wifiClient);

#define FLIPDOT_STROBE_UPDATE_INTERVAL_SEC 1000 * 60 * 5
uint32_t flipdot_strobe_cur=0, flipdot_strobe_prev=0;

// Update time from NTP server every hour
#define NTP_UPDATE_INTERVAL_SEC 1000*60*60
uint32_t ntp_update_cur=0, ntp_update_prev=0;

// NTP time display update (only when idle)
#define NTP_TIME_UPDATE_INTERVAL_SEC 1000 * 20
uint32_t ntp_time_cur=0, ntp_time_prev=0;

// idle timer
#define IDLE_UPDATE_INTERVAL_SEC 1000 * 30
uint32_t idle_time_cur=0, idle_time_prev=0;

boolean idleTimerIsRunning = false;

void startIdleTimer() {
  idle_time_cur = idle_time_prev = millis();
  idleTimerIsRunning = true;
}

void stopIdleTimer() {
  idleTimerIsRunning = false;
}

boolean checkIdleTimer() {
  boolean retval = false;
  
  if( idleTimerIsRunning ) {
    idle_time_cur = millis();
    uint32_t idle_time_elapsed = idle_time_cur - idle_time_prev;
    if( idle_time_elapsed >= IDLE_UPDATE_INTERVAL_SEC ) {
      retval = true;
    }
  }  
  return retval;
}

// NTP time/date servers in NL
#define NTP_SERVERS "0.nl.pool.ntp.org", "1.nl.pool.ntp.org", "2.nl.pool.ntp.org"
#define timezone 1 // Central Europe, Time Zone (Amsterdam, the Netherlands)
struct dstRule StartRule = {"CEST", Last, Sun, Mar, 2, 3600};    // Daylight time = UTC/GMT +2 hours
struct dstRule EndRule = {"CET", Last, Sun, Oct, 1, 0};          // Standard time = UTC/GMT +1 hours
simpleDSTadjust dstAdjusted(StartRule, EndRule);

// Some LOGO displayed at boot
static const uint8_t PROGMEM ilovedots [] = {
0x00, 0x0f, 0xf0, 0x3c, 0x3c, 0x0f, 0xe0, 0x03, 0xf0, 0x3f, 0xff, 0x07, 0xe0, 0x00, 0x00, 0x0f, 
0xf0, 0x7e, 0x7e, 0x0f, 0xf8, 0x07, 0xf8, 0x3f, 0xff, 0x1f, 0xf0, 0x00, 0x00, 0x0f, 0xf0, 0xff, 
0xff, 0x0f, 0xfc, 0x0f, 0xfc, 0x3f, 0xff, 0x1f, 0xf0, 0x00, 0x00, 0x03, 0xc1, 0xe7, 0xe7, 0x8e, 
0x3e, 0x1f, 0x3e, 0x3f, 0xff, 0x3c, 0x30, 0x00, 0x00, 0x03, 0xc1, 0xc3, 0xc3, 0x8e, 0x1e, 0x1e, 
0x1e, 0x01, 0xe0, 0x3c, 0x10, 0x00, 0x00, 0x03, 0xc1, 0xc1, 0x83, 0x8e, 0x0f, 0x3c, 0x0f, 0x01, 
0xe0, 0x3e, 0x00, 0x00, 0x00, 0x03, 0xc1, 0xc0, 0x03, 0x8e, 0x0f, 0x3c, 0x0f, 0x01, 0xe0, 0x3f, 
0xc0, 0x00, 0x00, 0x03, 0xc1, 0xe0, 0x07, 0x8e, 0x0f, 0x3c, 0x0f, 0x01, 0xe0, 0x1f, 0xf0, 0x00, 
0x00, 0x03, 0xc0, 0xe0, 0x07, 0x0e, 0x0f, 0x3c, 0x0f, 0x01, 0xe0, 0x0f, 0xf0, 0x00, 0x00, 0x03, 
0xc0, 0xf0, 0x0f, 0x0e, 0x0f, 0x3c, 0x0f, 0x01, 0xe0, 0x03, 0xf8, 0x00, 0x00, 0x03, 0xc0, 0x78, 
0x1e, 0x0e, 0x0f, 0x3c, 0x0f, 0x01, 0xe0, 0x00, 0xf8, 0x00, 0x00, 0x03, 0xc0, 0x3c, 0x3c, 0x0e, 
0x1e, 0x1e, 0x1e, 0x01, 0xe0, 0x20, 0x78, 0x00, 0x00, 0x03, 0xc0, 0x3e, 0x7c, 0x0e, 0x3e, 0x1f, 
0x3e, 0x01, 0xe0, 0x30, 0x78, 0x00, 0x00, 0x0f, 0xf0, 0x0f, 0xf0, 0x0f, 0xfc, 0x0f, 0xfc, 0x01, 
0xe0, 0x3f, 0xf0, 0x00, 0x00, 0x0f, 0xf0, 0x07, 0xe0, 0x0f, 0xf8, 0x07, 0xf8, 0x01, 0xe0, 0x3f, 
0xe0, 0x00, 0x00, 0x0f, 0xf0, 0x01, 0x80, 0x0f, 0xe0, 0x03, 0xf0, 0x01, 0xe0, 0x0f, 0xc0, 0x00
};

// mqtt topic strings: flipdot/<uid>/msg and flipdot/<uid>/msg
char mqtt_topic_raw[64];
char mqtt_topic_msg[64];

// RS485 Hanover Flipdot display
HardwareSerial Serial1(1);

//
//
//
void setup() {

  // Init Serial port
  Serial.begin(115200, SERIAL_8N1);
  Serial.printf("\n\r... in debug mode ...\n\r");

  // Hanover rs485 interface
  Serial1.begin(4800);  // pin 10=TX, rx not used

  // Test is SPIFFS is already formatted
  if(!SPIFFS.begin()){
    SPIFFS.begin(true);
    Serial.printf("Format SPIFFS ...\n\r");
    ESP.restart();
  }

  // Define I/O pins
  pinMode(RST_PIN, INPUT);

  // Flipdot
  flipdot.setRotation(0); // 0 = landscape 0 , 2 = landscape 180 1 = portrait 0 ,3 = portrait 180
  flipdot.clr();
  flipdot.drawPixel(flipdot.getWidthInPixels()/2, flipdot.getHeightInPixels()/2, YELLOW);
  flipdot.display();
  
  // Backlight
  bl_init();

  // Setup unique mqtt id and mqtt topic string
  sprintf(app_config.mqtt_id,"HFD_112x16_01_%08X",ESP.getEfuseMac()); 
  sprintf(mqtt_topic_raw,"DotMessage/HFD-%08X/raw",ESP.getEfuseMac());
  sprintf(mqtt_topic_msg,"DotMessage/HFD-%08X/msg",ESP.getEfuseMac());
  
  // Init with red led
  smartLedInit(red);
  for(int idx = 0; idx < 5; idx++ ) {
    smartLedFlash(colorFlashLevel);
    delay(100);
  }
  
  // Perform factory when reset
  // is pressed during powerup
  if( 0 == digitalRead(RST_PIN) ) {
    DEBUG_PRINTF("%s:RST pushbutton pressed, reset WiFiManager and appConfig settings\n", __FUNCTION__);
    //wifiManager.resetSettings(); // Bugs 
    deleteAppConfig(SPIFFS, "/config.json");

    // Bugfix:
    if( wifiManager.autoConnect("Hanover 112x16 Config") ) {
      WiFi.disconnect(true);
      delay(1000);
      ESP.restart();
    }
    DEBUG_PRINTF("%s:ssid erased\n", __FUNCTION__);
    
    while(0 == digitalRead(RST_PIN)) {
       smartLedFlash(colorFlashLevel);
       DEBUG_PRINTF("%s:Wait for user to release RST pin\n", __FUNCTION__);
       delay(500);
    }
    ESP.restart();
  }

  // Read config file or generate default
  if( !readAppConfig(SPIFFS, "/config.json", &app_config) ) {
    strcpy(app_config.mqtt_username, "");
    strcpy(app_config.mqtt_password, "");
    strcpy(app_config.mqtt_remote_host, "test.mosquitto.org");
    strcpy(app_config.mqtt_remote_port, "1883");
    writeAppConfig(SPIFFS, "/config.json", &app_config);
  }

  // Setup WiFiManager
  smartLedShowColor(blue);
  wifiManager.setMinimumSignalQuality(20);
  wifiManager.setTimeout(300);
  wifiManager.setSaveConfigCallback(saveConfigCallback);
  wifiManager.setDebugOutput(false);
  shouldSaveConfig = false;

  // Adds some parameters to the default webpage
  WiFiManagerParameter wmp_text("<br/>MQTT setting:</br>");
  wifiManager.addParameter(&wmp_text);
  WiFiManagerParameter custom_mqtt_username("mqtt_username", "Username", app_config.mqtt_username, MQTT_USERNAME_LENGTH);
  WiFiManagerParameter custom_mqtt_password("mqtt_password", "Password", app_config.mqtt_password, MQTT_PASSWORD_LENGTH);
  WiFiManagerParameter custom_mqtt_remote_host("mqtt_remote_host", "Host", app_config.mqtt_remote_host, MQTT_REMOTE_HOST_LENGTH);
  WiFiManagerParameter custom_mqtt_remote_port("mqtt_port", "Port", app_config.mqtt_remote_port, MQTT_REMOTE_PORT_LENGTH);
  wifiManager.addParameter(&custom_mqtt_username);
  wifiManager.addParameter(&custom_mqtt_password);
  wifiManager.addParameter(&custom_mqtt_remote_host);
  wifiManager.addParameter(&custom_mqtt_remote_port);

  // Add the unit ID to the webpage
  char fd_str[128];
  sprintf(fd_str, "<p>Your FlipDot Signature: <b>%08X</b> (You will need this later in the app)", ESP.getEfuseMac());
  WiFiManagerParameter mqqt_topic_text(fd_str);
  wifiManager.addParameter(&mqqt_topic_text);

  // Start WiFiManager ...
  if( !wifiManager.autoConnect("Hanover 112x16 Config")) {
    delay(1000);
    ESP.restart();
  }

  //
  // Update config if needed
  //
  if(shouldSaveConfig) {
    strcpy(app_config.mqtt_username, custom_mqtt_username.getValue());
    strcpy(app_config.mqtt_password, custom_mqtt_password.getValue());
    strcpy(app_config.mqtt_remote_host, custom_mqtt_remote_host.getValue());
    strcpy(app_config.mqtt_remote_port, custom_mqtt_remote_port.getValue());
    writeAppConfig(SPIFFS, "/config.json", &app_config);
  }

  // Setup OTA
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  // ArduinoOTA.setHostname("myesp8266");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  //
  DEBUG_PRINTF("%s:Setting MDNS service to 'Hanover_112x16_flipdot_v10'\n",__FUNCTION__);
  if( !MDNS.begin("Hanover_112x16_flipdot_v10") ) {
  } else {
    MDNS.addService("Hanover_112x16_flipdot_v10", "tcp", 10000);
  }

  // Sync to NTP service
  DEBUG_PRINTF("%s:Sync to time server\n", __FUNCTION__);
  updateNTP();

  // Start idle timer
  startIdleTimer();

  // Start OverTheAir update services
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    DEBUG_PRINTF("%s:Start updating %s ...\n",__FUNCTION__, type.c_str());
  });
  
  ArduinoOTA.onEnd([]() {
    DEBUG_PRINTF("\n%s:End",__FUNCTION__);
  });
  
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    DEBUG_PRINTF("Progress: %u%%\r", (progress / (total / 100)));
  });
    
  ArduinoOTA.onError([](ota_error_t error) {
    DEBUG_PRINTF("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) DEBUG_PRINTF("%s:Auth Failed\n",__FUNCTION__);
    else if (error == OTA_BEGIN_ERROR) DEBUG_PRINTF("%s:Begin Failed",__FUNCTION__);
    else if (error == OTA_CONNECT_ERROR) DEBUG_PRINTF("%s:Connect Failed",__FUNCTION__);
    else if (error == OTA_RECEIVE_ERROR) DEBUG_PRINTF("%s:Receive Failed",__FUNCTION__);
    else if (error == OTA_END_ERROR) DEBUG_PRINTF("%s:End Failed",__FUNCTION__);
  });

  ArduinoOTA.begin();

  // Debug print
  DEBUG_PRINTF("%s:OAT ready\n", __FUNCTION__);
  DEBUG_PRINTF("%s:mqtt_username: %s\n", __FUNCTION__, app_config.mqtt_username);
  DEBUG_PRINTF("%s:mqtt_password: %s\n", __FUNCTION__, app_config.mqtt_password);
  DEBUG_PRINTF("%s:mqtt_id: %s\n", __FUNCTION__, app_config.mqtt_id);
  DEBUG_PRINTF("%s:mqtt_topic RAW: %s\n", __FUNCTION__, mqtt_topic_raw);
  DEBUG_PRINTF("%s:mqtt_topic MSG: %s\n", __FUNCTION__, mqtt_topic_msg);
  DEBUG_PRINTF("%s:mqtt_remote_host: %s\n", __FUNCTION__, app_config.mqtt_remote_host);
  DEBUG_PRINTF("%s:mqtt_remote_port: %s\n", __FUNCTION__, app_config.mqtt_remote_port);

  // Display powerup info on flipdot
  flipdot_show_powerup();

  // Set statusled to GREEN
  smartLedInit(green);
}

//
// 
//
void flipdot_show_powerup()
{
  flipdot.clr();
  flipdot.display();
  
  // Print IP
  char str[40];

  flipdot.clr();
  flipdot.setCursor(0,0);
  sprintf(str, "IP:\n%s", WiFi.localIP().toString().c_str());  
  flipdot.println(str);
  flipdot.display();
  delay(1500);

  flipdot.clr();
  flipdot.setCursor(0,0);
  sprintf(str, "MQTT:\n%s", app_config.mqtt_remote_host);
  flipdot.println(str);
  flipdot.display();
  delay(1500);

  // Set flipdot image
  flipdot.clr();
  flipdot.drawBitmap(0, 0, ilovedots, 112, 16);
  flipdot.display();
  delay(1500);
}

//
//
//
void strobeDisplayHandler() {
  flipdot.invert();
  flipdot.display();

  flipdot.invert();
  flipdot.display();
}

//
// MQTT callback
//
#define DISPLAY_WIDTH 112
#define DISPLAY_HEIGHT 16

void mqtt_callback(char* topic, byte* payload, unsigned int length) {

  // 
  // Raw Message (pixels)
  // 
  if( 0 == strcmp(topic, mqtt_topic_raw) ) {

    // Reset timer
    startIdleTimer();

    //
    uint8_t buf[ DISPLAY_WIDTH * DISPLAY_HEIGHT/8 ];
    uint8_t *pbuf = buf;
    
    DynamicJsonBuffer jsonBuffer(2048);
    //StaticJsonBuffer<2048> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(payload);
    if( root.success() ) {
      JsonArray& pixels = root["pixels"]; 
      for( JsonArray::iterator it = pixels.begin(); it!=pixels.end(); ++it) {
        *pbuf++ = it->as<uint8_t>();
      }
    }
    flipdot.directDrawBuffer(buf);
    flipdot.display();

    // Flash the pixel
    smartLedFlash(colorFlashLevel);
  }

  // 
  // Normal Text
  // 
  if( 0 == strcmp(topic, mqtt_topic_msg) ) {

    // Reset timer
    startIdleTimer();

    // Parse message and display
    DynamicJsonBuffer jsonBuffer(2048);
    JsonObject& root = jsonBuffer.parseObject(payload);
    if( root.success() ) {

      // Test for message and flipdot message
      JsonVariant msg = root["message"];
      if(msg.success()) {
        flipdot.clr();
        flipdot.setCursor(0,0);
        flipdot.println(msg.as<const char*>());
        flipdot.display();
      }
      
      // Test for backlight and set ON/OFF
      JsonVariant bl = root["backlight"];
      if(bl.success()) {
        if( bl.as<uint8_t>() == 0 ) {
          bl_off();
        } else {
          bl_on();
        }
      }

      // Test for backlight auto On/Off
      JsonVariant blao = root["backlightAutoOff"];
      if(blao.success()) {
        if( blao.as<uint8_t>() == 0 ) {
          backlightAutoOff = false;
        } else {
          backlightAutoOff = true;          
        }
      }

      // Flash the pixel
      smartLedFlash(colorFlashLevel);
    }
  }
}

//
//
//
void loop() {

  // Check for IP connection
  if( WiFi.status() == WL_CONNECTED) {

    // Check for OTA firmware updates
    ArduinoOTA.handle();

    // Handle mqtt
    if( !mqttClient.connected() ) {
      mqtt_connect();
      delay(250);
    } else {
      // Handle MQTT loop
      mqttClient.loop();
    }
  } 

  // 
  // Always refresh display after STROBE_DISPLAY_UPDATE_MS
  //
  flipdot_strobe_cur = millis();
  uint32_t elapsed = flipdot_strobe_cur - flipdot_strobe_prev;
  if( elapsed >= FLIPDOT_STROBE_UPDATE_INTERVAL_SEC ) {
    flipdot_strobe_prev = flipdot_strobe_cur; 
    strobeDisplayHandler();
  }

  // 
  // Sync time with NTP
  //
  if( WiFi.status() == WL_CONNECTED) {
    ntp_update_cur = millis();
    uint32_t ntp_update_elapsed = ntp_update_cur - ntp_update_prev;
    if( ntp_update_elapsed >= NTP_UPDATE_INTERVAL_SEC ) {
      ntp_update_prev = ntp_update_cur; 
      updateNTP();
    }
  }

  // 
  // second timer
  //
  ntp_time_cur = millis();
  uint32_t ntp_time_elapsed = ntp_time_cur - ntp_time_prev;
  if( ntp_time_elapsed >= NTP_TIME_UPDATE_INTERVAL_SEC ) {
    ntp_time_prev = ntp_time_cur; 
    if( checkIdleTimer() ) {
      updateFlipDot();
    }
  }
}

/******************************************************************/
void updateFlipDot()
/* 
short:      Update flipdot
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
  char buf[30];
  char *dstAbbrev;
  time_t t = dstAdjusted.time(&dstAbbrev);
  struct tm *timeinfo = localtime (&t);
  int hour = (timeinfo->tm_hour+11)%12+1;  // take care of noon and midnight

  //sprintf(buf, "%02d/%02d/%04d %02d:%02d:%02d%s %s\n",timeinfo->tm_mon+1, timeinfo->tm_mday, timeinfo->tm_year+1900, hour, timeinfo->tm_min, timeinfo->tm_sec, timeinfo->tm_hour>=12?"pm":"am", dstAbbrev);
  //sprintf(buf, "%d:%02d:%02d %s",hour, timeinfo->tm_min, timeinfo->tm_sec, timeinfo->tm_hour>=12?"PM":"AM");
  //sprintf(buf, "%d:%02d:%02d",timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
  sprintf(buf, "%d:%02d",timeinfo->tm_hour, timeinfo->tm_min);
  DEBUG_PRINTF("%s: %s\n", __FUNCTION__, buf);
  
  flipdot.clr();

  // Random position
  uint8_t random_x = random(28*4 - 30);
  uint8_t random_y = random(16-7);
  flipdot.setCursor(random_x, random_y);

  flipdot.println(buf);
  flipdot.display();

  // Handle Backlight. Always turn off except 
  // when backlightAutoOff == false;
  if( backlightAutoOff == true ) {
    bl_off();
  }
}

/******************************************************************/
void mqtt_connect() 
/* 
short:      Connect to MQTT server UNSECURE
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
  char *host = app_config.mqtt_remote_host;
  int port = atoi(app_config.mqtt_remote_port);
  
  mqttClient.setClient(wifiClient);
  mqttClient.setServer(host, port);
  if(mqttClient.connect(app_config.mqtt_id)){

    // Subscribe to ../raw and ../msg
    mqttClient.subscribe(mqtt_topic_raw);
    mqttClient.subscribe(mqtt_topic_msg);

    // Set callback
    mqttClient.setCallback(mqtt_callback);
    DEBUG_PRINTF("%s: MQTT connected to %s:%d\n", __FUNCTION__, host, port);
  } else {
    DEBUG_PRINTF("%s: MQTT connection ERROR (%s:%d)\n", __FUNCTION__, host, port);
  }
}

/******************************************************************/
void updateNTP()
/* 
short:      Sync to NTP server    
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
  configTime(timezone * 3600, 0, NTP_SERVERS);
  delay(500);
  while (!time(nullptr)) {
    delay(500);
  }
}

/******************************************************************/
void printTime(time_t offset)
/* 
short:         
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
  char buf[30];
  char *dstAbbrev;
  time_t t = dstAdjusted.time(&dstAbbrev)+offset;
  struct tm *timeinfo = localtime (&t);
  
  int hour = (timeinfo->tm_hour+11)%12+1;  // take care of noon and midnight
  sprintf(buf, "%02d/%02d/%04d %02d:%02d:%02d%s %s\n",timeinfo->tm_mon+1, timeinfo->tm_mday, timeinfo->tm_year+1900, hour, timeinfo->tm_min, timeinfo->tm_sec, timeinfo->tm_hour>=12?"pm":"am", dstAbbrev);
  Serial.print(buf);
}

/******************************************************************/
/*
 * Smart LED section
 */
/******************************************************************/
 
/******************************************************************/
void smartLedShowColor(RgbColor color)
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
  // Set new color
  neoPixel.SetPixelColor(0, color);
  neoPixel.Show(); 
}

/******************************************************************/
void smartLedFlash(uint8_t level)
/* 
short:      Flash current color         
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
  RgbColor old = neoPixel.GetPixelColor(0);
  RgbColor flash = old;
  if(0!=old.R) flash.R = level;
  if(0!=old.G) flash.G = level;
  if(0!=old.B) flash.B = level;
  neoPixel.SetPixelColor(0, flash);
  neoPixel.Show();
  delay(50);
  neoPixel.SetPixelColor(0, old);
  neoPixel.Show();
}

/******************************************************************/
void smartLedInit(RgbColor color)
/* 
short:      Init         
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
  neoPixel.Begin();
  smartLedShowColor(color);
}

/******************************************************************/
/*
 * Backlight function
 */
/******************************************************************/

/******************************************************************/
void bl_init(void)
/* 
short:      Init backlight (lightstrip or TL in original Hanover FP)        
inputs:        
outputs: 
notes:      
Version :   DMK, Initial code
*******************************************************************/
{
  pinMode(BL_PIN, OUTPUT);
  bl_off();
  backlightAutoOff = true;
}


/******************************************************************/
void bl_on(void)
/* 
short:      turn BL on         
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
  digitalWrite(BL_PIN, HIGH);
}

/******************************************************************/
void bl_off(void)
/* 
short:      turn BL off
inputs:        
outputs: 
notes:         
Version :   DMK, Initial code
*******************************************************************/
{
  digitalWrite(BL_PIN, LOW);
}


/******************************************************************/
/*
 * Application signature and config
 */
/******************************************************************/

/******************************************************************/
void saveConfigCallback () 
/* 
short:         
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
  shouldSaveConfig = true;
}

/******************************************************************/
bool readAppConfig(fs::FS &fs, const char *path, APP_CONFIG_STRUCT *app_config) 
/* 
short:         Read config struct from flash
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
  bool retval = false;

  File file = fs.open(path);
  
  if( !file || file.isDirectory() )
  {
    DEBUG_PRINTF("%s: Failed to open (%s)\n", __FUNCTION__, path);
    return retval;
  }

  if( file ) {
    size_t size = file.size();
    std::unique_ptr<char[]> buf(new char[size]);
    
    file.readBytes(buf.get(), size);
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.parseObject(buf.get());
    if( json.success() ) {
       strcpy(app_config->mqtt_username, json["MQTT_USERNAME"]);
       strcpy(app_config->mqtt_password, json["MQTT_PASSWORD"]);
       strcpy(app_config->mqtt_remote_host, json["MQTT_HOST"]);
       strcpy(app_config->mqtt_remote_port, json["MQTT_PORT"]);
       retval = true;
    }
  } 
  
  return retval;
}

/******************************************************************/
bool writeAppConfig(fs::FS &fs, const char *path, APP_CONFIG_STRUCT *app_config) 
/* 
short:         Write config to FFS
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
  bool retval = false;
  
  File file = fs.open(path, FILE_WRITE);
  if(!file){
      DEBUG_PRINTF("%s: failed to open file (%s) for writing\n", __FUNCTION__, path);
      return retval;
  }

  // Create new and store settings
  DynamicJsonBuffer jsonBuffer;
  JsonObject& json = jsonBuffer.createObject();
  json["MQTT_USERNAME"] = app_config->mqtt_username;
  json["MQTT_PASSWORD"] = app_config->mqtt_password;
  json["MQTT_HOST"] = app_config->mqtt_remote_host;
  json["MQTT_PORT"] = app_config->mqtt_remote_port;

  char buf[1024];
  json.printTo(buf);
  
  if(file.print(buf)){
      retval = true;
  } else {
      DEBUG_PRINTF("%s: failed to write to (%s)\n", __FUNCTION__, path);
  } 
  
  return retval;
}

/******************************************************************/
void deleteAppConfig(fs::FS &fs, const char * path) 
/* 
short:         Delete confif from SPIFFS
inputs:        
outputs: 
notes:         
Version :      DMK, Initial code
*******************************************************************/
{
  if(!fs.remove(path)){
    DEBUG_PRINTF("%s: failed to delete (%s)\n", __FUNCTION__, path);
  }
}

