
# Dot Message board (Proof of concept, MEAN stack, MQTT, WS, ESP32, HT16K33) #

We all use WhatsApp group messages and that’s fine. But wouldn’t it be great to see your  group messages on a physical message board? And wouldn’t it be handy that selectable (AI) services push information to your groups message board?

The message board platform is built with 2018-IoT-Technology-That-Matters including MQTT, WebSocket’s, Restful, ESP32 and MEAN stack. All kinds of displays can be interfaced.

[![Alt text](https://img.youtube.com/vi/SYSjgnGJuGM/0.jpg)](https://www.youtube.com/watch?v=SYSjgnGJuGM)

and with cheap LED matrix boards (scrolling over multiple modules took some effort ;)

[![Alt text](https://img.youtube.com/vi/6p_Dj9Kuoa4/0.jpg)](https://youtu.be/6p_Dj9Kuoa4)

---
# The big picture
---

---
## ESP32
---
A picture is worth a thousand words
![Scheme](images/overview.PNG)

The ESP32-Pico-Kit V4 (ESP32) drives a display (the HCI part). In this repository implementation code for a retro Hanover flip-dot display and a modern LED matrix display is given. (The LED matrix display is a collection of Adafruit’s HT16K33 led matrix boards). The LED display has scrolling capabilities where the flipdot has more pixels. Bottom line is that the ESP32 can communicate messages to humans 😉

To provide user-feed back a WS2812B Neopixel is connected to IO2. To reset (at powerup) TCP/IP settings an pushbutton is connected to IO4. 

![Scheme](images/ipreset.png)
![Scheme](images/ws2812B.png)

Program flow @ power-up 

![Scheme](images/boot.png)


Just after the setup the ESP32 generate an unique signature (e.g. ```HFD-401DA0D8```) that is shown at powerup and during the internet setup procedure. With this signature the ESP32 connects to a MQTT broker (can be set during internet setup) and subscribes to a ```raw``` and a ```msg``` topic. Internet setup is done with the excellent ```WiFiManager``` package.

During boot the ESP32 communicates all settings to the serial console, e.g.:  

```
#!node
setup:mqtt_username: 
setup:mqtt_password: 
setup:mqtt_id: HFD_112x16_01_401DA0D8
setup:mqtt_topic RAW: DotMessage/HFD-401DA0D8/raw
setup:mqtt_topic MSG: DotMessage/HFD-401DA0D8/msg
setup:mqtt_remote_host: test.mosquitto.org
setup:mqtt_remote_port: 1883
mqtt_connect: MQTT connected to test.mosquitto.org:1883
```

The ```msg``` (message) topic is for publishing text-messages, the ```raw``` topic is for displaying bitmaps (e.g. images)

The ESP32 LED matrix version is capable of scrolling text messages. Scrolling is implemented using a table driven statemachine. This makes scrolling messages very easy to tune (e.g. delay new displayed messages before scrolling or message-length dependent scrolling speed). 

The statemachine design:

![Scheme](images/ScrollingTextStateMachine.png | width=250)

In code this looks like:

![Scheme](images/ScrollStateMachineArduino.PNG | width=250)
Checkout https://bitbucket.org/dkroeske/DotMessage/src/master/led_esp32/led_esp32.ino?fileviewer=file-view-default#led_esp32.ino-122 see all the details. ( Aargh, function pointers involved )

---
## The webapp
---
A backend serves a web-app using MEAN stack technology. A very basic ```Bootstrap v4.0``` based UI makes the internals the Angular 5 frontend more understandable.

The web app comes with two components. One to gather name, email and the unique ESP32 signature, the other to send messages. Navigating to the latter is only possible with valid (user)input data. The components are built with strict checks on input data using Angular 5 reactive forms and input validation using regular expressions. Input data is saved in the Angular localStorage.

When navigation to the Message component connected clients also connect to the backend’s websocket interface. This makes it possible to instantaneous update all the DOM’s of all connected clients. This means that send messages show up on all client with the same ESP32 signature (e.g. within the same group).

At startup the Message component retrieves the last 3 send messages using a restful api call and render the result in the client’s DOM.

![Scheme](images/chatok.png) ![Scheme](images/chat.png)


---
## The backend
---
The Node.js backend connects at boot to a local installed Mongo database and stores all send messages. The web-app sends messages by POST api calls. The backend publishes part of posted messages to a MQTT broker meaning all subscribed ESP’s can display posted messages.

The node backend acts as a webserver (Express) to serve the Angular 5 webapp

The source tree contains a python script that can publish bitmap data to the ```raw``` topic (E.g. a battery icon combined with a 'car fully charged' message). Other backend processes can use this example (e.g. 'The build broke by ....')

---
# Installation and deployment
---

Assuming npm, NodeJS and Mongo are installed: clone all code in an app directory and run ```npm install``` to install all dependencies (see ```package.json```). The command ```npm server.js``` runs the backend and the Angular 5 webapp. The webapp runs at port **8080**. (Better: use a processmanager like ```PM2``` or even better deployment software like ```docker```)

---
## User friendly internet setup (connecting the a WiFi accesspoint)
---
This is implementated using the ```[WiFiManager](https://github.com/zhouhan0126/WIFIMANAGER-ESP32)``` package. When no previous internet setting is avaliable the ESP32 boots in HOTSPOT mode. Connect to the hotspot (with ssid: ```LEDMatrix Config```) and a simple website is provided to fill-in credentials of available accesspoints. After successful connection the given credentials (and perhaps other settings) are non-volatile stored and the ESP32 will reboot. If all went ok the ESP32 connects as client.

![Scheme](images/IP_config.jpg | width=250)

To reset all settings the button needs to be pressed at powerup, the red led will blink;

---
## LED status
---

The WS2812B smart led (neopixel) provided some basic diagnostic information:

* RED 			: Boot or lost internet connecting
* RED blinking	: 3 times at boot or during powerup with settings-reset-key pressed.
* BLUE			: Trying to connect to the internet
* GREEN			: Connected to the internet (and the mqqt broker)
* GREEN flashes	: MQTT topic data updated.

---
## Testing and tools used
---

* The ESP32 Arduino Port for rapid prototyping. I had some troubles with timing and the WiFiClientSecure. I'm sure this will be fixed in later releases.
* Angular 5 / Mongoose / NodeJS / npm for the backend. The MEAN stack is amazing.
* test.mosquitto.org as MQTT broker. MQTT Lens and Linux commandline tools are used to debug MQTT connection, certificates and topic publish/subscribe. Mocca is used for unit testing
* All tools used are provided and maintained by the open source community. Thx a lot!

![Scheme](images/mqqt_lens.png)
![Scheme](images/mqtt_test.png)


---
## License
 
The MIT License (MIT)

Copyright (c) 2018 Diederich Kroeske

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



